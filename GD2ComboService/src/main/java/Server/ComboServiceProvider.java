package Server;

import Core.ComboServiceDetails;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.Date;

public class ComboServiceProvider
{
    public static void main(String[] args)
    {
        //Variable to track the number of requests
        int countRequests = 0;
        boolean continueRunning = true;
        //Set up maximum payload
        final int MAX_LEN = 150;

        DatagramSocket providerSocket = null;

        try
        {
            //Create a socket to send and receive messages
            providerSocket = new DatagramSocket(ComboServiceDetails.providerListeningPort);

            System.out.println("Listening on port: " + ComboServiceDetails.providerListeningPort);

            //Loop forever and just process requests
            while(continueRunning)
            {
                //Create a buffer to hold the input message
                byte[] incomingMessageBuffer = new byte[MAX_LEN];
                DatagramPacket incomingPacket = new DatagramPacket(incomingMessageBuffer, incomingMessageBuffer.length);

                //Wait to receive a message
                //This is blocking
                providerSocket.receive(incomingPacket);
                //Get the data out of the packet
                String data = new String(incomingPacket.getData());

                //Process the data, trim off any excess white space
                data = data.trim();

                //Break the message up based on breaking character
                String[] messageComponents = data.split(ComboServiceDetails.breakingCharacters);
                countRequests++;

                String response = null;

                //Work out what command was sent and respond appropriately
                if(messageComponents[0].equalsIgnoreCase("echo"))
                {
                    //Strip out the echo and the breaking characters and bounce the remainder back in the response
                    response = data.replace("echo" + ComboServiceDetails.breakingCharacters, "");
                }
                //If the first word is daytime
                else if(messageComponents[0].equalsIgnoreCase("daytime"))
                {
                    response = new Date().toString();
                }
                else if(messageComponents[0].equalsIgnoreCase("stats"))
                {
                    response = "Number of requests dealt with by the server is: " + countRequests;
                }
                else
                {
                    response = "Unrecognised request";
                }

                //Get address of the sender
                InetAddress requesterAddress = incomingPacket.getAddress();

                byte[] responseBuffer = response.getBytes();
                //Create a packet to store the response
                DatagramPacket responsePacket = new DatagramPacket(responseBuffer, responseBuffer.length, requesterAddress, ComboServiceDetails.requesterListeningPort);

                //Send the response
                providerSocket.send(responsePacket);
            }
        }
        catch(SocketException e)
        {
            System.out.println(e.getMessage());
        }
        catch(IOException ioe)
        {
            System.out.println(ioe.getMessage());
        }
        finally
        {
            if(providerSocket != null)
            {
                providerSocket.close();
            }
        }
    }
}
