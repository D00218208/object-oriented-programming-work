package Core;

public class ComboServiceDetails
{
    public static final int requesterListeningPort = 50001;
    public static final int providerListeningPort = 50000;
    public static final String breakingCharacters = "&&";
    public static final int MAX_LEN = 150;
}
