package Client;

import Core.ComboServiceDetails;

import java.io.IOException;
import java.net.*;
import java.util.InputMismatchException;
import java.util.Scanner;

public class ComboServiceConsumer
{
    public static void displayMenu()
    {
        System.out.println("Please enter one of the following options: ");
        System.out.println("1) Echo a message");
        System.out.println("2) Get the current date and time");
        System.out.println("3) Get number of requests to server");
        System.out.println("Enter -1 to end the program");
    }

    public static int getChoice(Scanner input)
    {
        int choice = 0;
        boolean validNumber = false;
        while(!validNumber)
        {
            try
            {
                choice = input.nextInt();
                validNumber = true;
            }
            catch(InputMismatchException ime)
            {
                System.out.println("Please enter a number from the menu");
                System.out.println();
                displayMenu();
            }
        }
        return choice;
    }

    public static void main(String[] args)
    {
        //Set up scanner so that we can interact
        Scanner input = new Scanner(System.in);

        //Set up a socket so that we can use to send and receive
        DatagramSocket requesterSocket = null;

        try
        {
            //Set up the address information of the server
            InetAddress serviceProviderHost = InetAddress.getByName("localhost");

            //Set up the DatagramSocket for sending and receiving
            requesterSocket = new DatagramSocket(ComboServiceDetails.requesterListeningPort);

            boolean continueRunning = true;

            while(continueRunning)
            {
                displayMenu();

                int choice = getChoice(input);
                String message = null;
                boolean sendMessage = true;

                switch(choice)
                {
                    case -1:
                        continueRunning = false;
                        sendMessage = false;
                        break;
                    case 1:
                        input.nextLine();
                        System.out.println("What message do you want to echo: ");
                        message = "echo" + ComboServiceDetails.breakingCharacters+input.nextLine();
                        break;
                    case 2:
                        message = "daytime";
                        break;
                    case 3:
                        message = "stats";
                        break;
                    default:
                        System.out.println("Bad request. Please choose from the menu of options");
                        sendMessage = false;
                }
              if(sendMessage)
              {
                  //Process the message to send
                  byte buffer[] = message.getBytes();

                  //Build DatagramPacket
                  //buffer - byte array to send
                  //buffer.length - length of buffer
                  //serviceProviderHost - IP address of the server
                  //providerListeningPort - port of the server
                  DatagramPacket requestPacket = new DatagramPacket(buffer, buffer.length, serviceProviderHost, ComboServiceDetails.providerListeningPort);

                  //Send  the datagram
                  requesterSocket.send(requestPacket);
                  System.out.println("Message sent");

                  //Receive the response
                  //Create a buffer
                  byte[] responseBuffer = new byte[ComboServiceDetails.MAX_LEN];
                  DatagramPacket responsePacket = new DatagramPacket(responseBuffer, responseBuffer.length);

                  //Wait to receive a response
                  requesterSocket.receive(responsePacket);
                  //Get data out of packet
                  String data= new String(responsePacket.getData());
                  System.out.println("Response: " + data.trim() + ".");
              }
                System.out.println();
            }
            System.out.println("Thank you for using the Combo Service program");
        }
        catch(SocketException se)
        {
            System.out.println(se.getMessage());
        }
        catch(UnknownHostException uhe)
        {
            System.out.println(uhe.getMessage());
        }
        catch(IOException ioe)
        {
            System.out.println(ioe.getMessage());
        }
        finally
        {
            if(requesterSocket != null)
            {
                requesterSocket.close();
            }
        }
    }
}
