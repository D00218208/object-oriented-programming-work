package com.dkit.gd2.patricknugent.game;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Monster implements ISavable
{
    private String name;
    private int hitPoints;
    private int strength;

    public Monster(String name, int hitPoints, int strength)
    {
        this.name = name;
        this.hitPoints = hitPoints;
        this.strength = strength;
    }

    public String getName()
    {
        return name;
    }

    public int getHitPoints()
    {
        return hitPoints;
    }

    public int getStrength()
    {
        return strength;
    }

    @Override
    public String toString()
    {
        return "Monster{" + "name=" + name + ", hitpoints=" + hitPoints + ", strength=" + strength + '}';
    }

    @Override
    public List<String> write()
    {
        return null;
    }

    @Override
    public void read(List<String> savedValues)
    {

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Monster)) return false;
        Monster monster = (Monster) o;
        return hitPoints == monster.hitPoints &&
                strength == monster.strength &&
                Objects.equals(name, monster.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, hitPoints, strength);
    }

}
