package com.dkit.gd2.patricknugent.game;

import java.util.List;

public interface ISavable
{
    List<String> write();
    void read(List<String> savedValues);
}