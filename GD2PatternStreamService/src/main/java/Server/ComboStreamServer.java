package Server;

import Core.ComboServiceDetails;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ComboStreamServer
{
    public static void main(String[] args)
    {
        ServerSocket listeningSocket = null;
        Socket dataSocket = null;
        try
        {
            listeningSocket = new ServerSocket(ComboServiceDetails.LISTENING_PORT);
            ThreadGroup group = new ThreadGroup("Client threads");
            group.setMaxPriority(Thread.currentThread().getPriority() -1);

            //Do the main logic of the server
            boolean continueRunning = true;
            //int threadCount = 0;
            while(continueRunning)
            {
                dataSocket = listeningSocket.accept();
                ComboServiceThread newClient = new ComboServiceThread(group, dataSocket.getInetAddress() + "", dataSocket);
                newClient.start();

                System.out.println("The server has now accepted " + group.activeCount());
            }
        }
        catch(IOException ioe)
        {
            System.out.println(ioe.getMessage());
        }
        finally
        {
            try
            {
                group.
                if(dataSocket != null)
                {
                    dataSocket.close();
                }
                if(listeningSocket != null)
                {
                    listeningSocket.close();
                }
            }
        }
    }
}
