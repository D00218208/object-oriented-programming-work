package Client;

import Core.ComboServiceDetails;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class ComboStreamClient
{
    public static void main(String[] args)
    {
        Socket dataSocket = null;
        try
        {
            //Step 1: Connect to server
            dataSocket = new Socket("localhost", ComboServiceDetails.LISTENING_PORT);

            //Step 2: Build the input output streams
            OutputStream out = dataSocket.getOutputStream();
            PrintWriter output = new PrintWriter(new OutputStreamWriter(out));

            InputStream in = dataSocket.getInputStream();
            Scanner input = new Scanner(new InputStreamReader(in));

            //Step 3: Get user input
            Scanner keyboard = new Scanner(System.in);
            String message = "";
            String response;
            while(!message.equals(ComboServiceDetails.END_SESSION))
            {
                displayMenu();
                int choice = getNumber(keyboard);

                if(choice > 0 && choice < 3)
                {
                    switch(choice)
                    {
                        case 0:
                            message = ComboServiceDetails.END_SESSION;
                            break;
                        case 1:
                            //ECHO%%The message to send
                            message = generateEcho(keyboard);
                            break;
                        case 2:
                            message = ComboServiceDetails.DAYTIME;
                            break;
                    }
                    //Step 4: Send to the server
                    output.println(message);
                    //output.flush(); Using autoflush
                    //Step 5: Wait for response
                    response = input.nextLine();
                    System.out.println(response);
                }
                else
                {
                    System.out.println("Please select 0, 1 or 2 from the menu");
                }
            }
            System.out.println("Thank you for using the Combo Service system");

        }
        catch(UnknownHostException uhe)
        {
            System.out.println(uhe.getMessage());
        }
        catch (IOException ioe)
        {
            System.out.println(ioe.getMessage());
        }
        finally
        {
            if(dataSocket != null)
            {
                try
                {
                    dataSocket.close();
                }
                catch (IOException ioe)
                {
                    System.out.println(ioe.getMessage());
                    System.exit(1);
                }
            }
        }
    }

    public static void displayMenu()
    {
        System.out.println("0) to quit");
        System.out.println("1) to echo");
        System.out.println("2) to get daytime");
    }

    public static  int getNumber(Scanner keyboard)
    {
        boolean numberEntered = false;
        int number = 0;
        while (!numberEntered)
        {
            try
            {
                number = keyboard.nextInt();
                numberEntered = true;
            }
            catch(InputMismatchException ime)
            {
                System.out.println("Please enter a number");
                keyboard.nextLine();
            }
        }
        keyboard.nextLine();
        return number;
    }

    public static String generateEcho(Scanner keyboard)
    {
        StringBuffer message = new StringBuffer(ComboServiceDetails.ECHO);
        message.append(ComboServiceDetails.COMMAND_SEPARATOR);
        System.out.println("What message would you like to echo?");
        String echo = keyboard.nextLine();
        //Add a language filter
        message.append(echo);
        return message.toString();
    }
}
