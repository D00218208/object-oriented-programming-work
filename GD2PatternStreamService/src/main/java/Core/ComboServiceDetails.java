package Core;

public class ComboServiceDetails
{
    public static final int LISTENING_PORT = 50000;

    //Breaking Characters
    public static final String COMMAND_SEPARATOR = "%%";

    //Command Strings
    public static final String END_SESSION = "QUIT";
    public static final String ECHO = "ECHO";
    public static final String DAYTIME = "DAYTIME";

    //Response Strings
    public static final String UNRECOGNISED = "UNKNOWN_COMMAND";
    public static final String SESSION_TERMINATED = "GOODBYE";
}
