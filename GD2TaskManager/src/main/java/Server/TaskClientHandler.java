package Server;

import Core.Task;
import Core.TaskManager;
import Core.TaskManagerServiceDetails;

import java.awt.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.sql.Date;
import java.util.Scanner;
import java.util.Set;

public class TaskClientHandler implements Runnable
{
    private Socket clientSocket;
    private TaskManager taskList;

    public TaskClientHandler(Socket clientSocket, TaskManager taskList)
    {
        this.clientSocket = clientSocket;
        this.taskList = taskList;
    }

    @Override
    public void run()
    {
        try
        {
            //Open the lines of communication
            //An example of the decorator pattern
            Scanner clientInput = new Scanner(clientSocket.getInputStream());
            PrintWriter clientOutput = new Scanner(clientSocket.getOutputStream());

            //Step 2: Set up repeated exchnages
            //Look at what the client sends and respond appropriately
            boolean sessionActive = true;
            while (sessionActive) {
                //Protocol logic
                String request = clientInput.nextLine();

                //Break up the request into components
                //add%%OOPCA6%%John%%1234567
                //exit
                //viewAll
                //remove%%OOPCA6
                String[] components = request.split(TaskManagerServiceDetails.BREAKING_CHARACTERS);
                String response = null;
                switch (components[0]) {
                    case TaskManagerServiceDetails.ADD_COMMAND:
                        response = generateAddResponse(components);
                        break;
                    case TaskManagerServiceDetails.REMOVE_COMMAND:
                        response = generateRemoveResponse(components);
                        break;
                    case TaskManagerServiceDetails.VIEW_COMMAND:
                        response = generateViewAllResponse(components);
                        break;
                    case TaskManagerServiceDetails.EXIT_COMMAND:
                        response = TaskManagerServiceDetails.SIGN_OFF;
                        sessionActive = false;
                        break;
                }
                if (response != null) {
                    clientOutput.println(response);
                }
            }
            clientSocket.close();
        }
        catch (IOException ioe)
        {
            System.out.println(ioe.getMessage());
        }
        //If the client sends add
        //If the client sends remove
        //If the client sends viewAll
        //If the client sends exit

    }

    public String generateAddResponse(String[] components)
    {
        //add%%OOPCA6%%John%%123456
        String response = null;
        if(components.length == TaskManagerServiceDetails.ARGUMENTS_FOR_ADD)
        {
            try
            {
                String taskName = components[TaskManagerServiceDetails.TASK_NAME_INDEX];
                String taskOwner = components[TaskManagerServiceDetails.TASK_OWNER_INDEX];
                long deadline = Long.parseLong(components[TaskManagerServiceDetails.TASK_DATE_INDEX]);

                Task newTask = new Task(taskName, taskOwner, new Date(deadline));
                boolean added = taskList.add(newTask);
                if(added)
                {
                    response = TaskManagerServiceDetails.SUCCESSFUL_ADD;
                }
                else
                {
                    response = TaskManagerServiceDetails.FAILED_ADD;
                }
            }
            catch (NumberFormatException nfe)
            {
                response = TaskManagerServiceDetails.FAILED_ADD;
            }
        }
        return response;
    }

    private String generateRemoveResponse(String [] components)
    {
        //remove%%OOPCA6
        String response = null;
        if(components.length == TaskManagerServiceDetails.ARGUMENTS_FOR_REMOVE)
        {
            String taskName = components[TaskManagerServiceDetails.TASK_NAME_INDEX];

            Task taskToBeRemoved = new Task(taskName);
            boolean removed = taskList.remove(taskToBeRemoved);
            if(removed)
            {
                response = TaskManagerServiceDetails.SUCCESSFUL_REMOVE;
            }
            else
            {
                response = TaskManagerServiceDetails.FAILED_REMOVE;
            }
        }
        return response;
    }

    public String generateViewAllResponse(String [] components)
    {
        //viewAll
        String response = null;
        if(components.length == TaskManagerServiceDetails.ARGUMENTS_FOR_VIEWALL)
        {
            Set<Task> tasks = taskList.getAllTasks();
            response = TaskManagerServiceDetails.flattenTaskSet(tasks);
            if(response == null)
            {
                response = "Dummy Task%%Dummy Owner%%" + new Date().getTime();
            }
        }
        return response;
    }
}
