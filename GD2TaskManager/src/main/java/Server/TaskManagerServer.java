package Server;

import Core.TaskManager;
import Core.TaskManagerServiceDetails;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class TaskManagerServer
{
    public static void main(String[] args)
    {
        TaskManager taskList = new TaskManager();
        ServerSocket listeningSocket = null;
        try
        {
            listeningSocket = new ServerSocket(TaskManagerServiceDetails.PORT_NUMBER);

            while (true)
            {
                Socket clientSocket = listeningSocket.accept();

                TaskClientHandler clientHandler = new TaskClientHandler(clientSocket, taskList);
                Thread worker = new Thread(clientHandler);
                worker.start();
            }
        }
        catch (IOException ioe)
        {
            System.out.println(ioe.getMessage());
        }
        finally
        {
            try
            {
                listeningSocket.close();
            }
            catch (IOException ioe)
            {
                System.out.println(ioe.getMessage());
                System.exit(1);
            }
        }
    }
}
