package Core;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class TaskManagerServiceDetails
{
    public final static String BREAKING_CHARACTERS = "%%";

    public final static String HOSTNAME = "localhost";
    public final static int PORT_NUMBER = 23400;

    public final static String ADD_COMMAND = "add";
    public final static String SUCCESSFUL_ADD = "ADDED";
    public final static String FAILED_ADD = "FAILED";

    public final static String REMOVE_COMMAND = "remove";
    public final static String SUCCESSFUL_REMOVE = "DELETED";
    public final static String FAILED_REMOVE = "NOT_FOUND";

    public final static String VIEW_COMMAND = "viewAll";
    public final static String TASK_SEPARATOR = "##";

    public final static String EXIT_COMMAND = "EXIT";
    public final static String SIGN_OFF = "GOODBYE";
    public final static int ARGUMENTS_FOR_ADD = 4;
    public final static int ARGUMENTS_FOR_REMOVE = 2;
    public final static int ARGUMENTS_FOR_VIEWALL = 1;
    public final static int TASK_NAME_INDEX = 1;
    public final static int TASK_OWNER_INDEX = 2;
    public static final int TASK_DATE_INDEX = 3;

    public static String flattenTaskSet(Set<Task> tasks)
    {
        if(!tasks.isEmpty())
        {
            StringBuilder taskString = new StringBuilder();
            //e.g task OOPCA6%%John%%12345##Web
            for(Task task : tasks)
            {
                taskString.append(task.format());
                taskString.append(TaskManagerServiceDetails.TASK_SEPARATOR);
            }
            return taskString.toString().substring(0, taskString.toString().length()-2);
        }
        return null;
    }

    public static Set<Task> recreateTaskSet(String taskString)
    {
        Set<Task> tasks = new HashSet<>();
        String [] taskStrings = taskString.split(TaskManagerServiceDetails.TASK_SEPARATOR);
        for(String task : taskStrings)
        {
            //Example task CA6%%ADAM%%123456789
            String[] components = task.split(TaskManagerServiceDetails.BREAKING_CHARACTERS);
            if(components.length == 3)
            {
                try
                {
                    long deadlineTime = Long.parseLong(components[2]);
                    Date deadline = new Date(deadlineTime);
                    Task t = new Task(components[0], components[1], deadline);
                    tasks.add(t);
                }
                catch (NumberFormatException nfe)
                {
                    System.out.println(nfe.getMessage());
                }
            }
        }
        return tasks;
    }
}
