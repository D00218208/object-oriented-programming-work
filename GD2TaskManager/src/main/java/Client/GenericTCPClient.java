package Client;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.sql.SQLOutput;
import java.util.Scanner;

public class GenericTCPClient
{
    //Take in input from the user in the exact format expected of the server
    //Ask the user to enter the ip address and port of the server
    //Connect to the server
    //Use a scanner to get the possible inputs
    //e.g add%%OOPCA6%%John%%12345556
    //remove%%OOPCA6
    //exit
    //viewAll
    //Send the command to the server
    //Get and print response
    public static void main(String[] args)
    {
        Scanner input = new Scanner(System.in);
        input.useDelimiter("\n");

        System.out.println("Please enter the hostname of the server: ");
        String hostname = input.next();
        System.out.println("Please enter the port number that the server is running on");
        int port = input.nextInt();

        try
        {
            Socket dataSocket = new Socket(hostname, port);

            Scanner serverIn = new Scanner(dataSocket.getInputStream());
            PrintWriter serverOut = new PrintWriter(dataSocket.getOutputStream(), true);

            boolean keepRunning = true;

            while (keepRunning) {
                System.out.println("Please enter a command to be sent");
                String toBeSent = input.next();

                //Send the message to the server
                serverOut.println(toBeSent);

                String response = serverIn.nextLine();
                System.out.println("Response: " + response);

                System.out.println("Do you wish to continue? (-1 to end or any other key to continue)");
                String choice = input.next();
                if (choice.equals("-1")) {
                    keepRunning = false;
                }
            }
            System.out.println("Thank you for using the Task Manager client");
        }
        catch (IOException ioe)
        {
            System.out.println(ioe.getMessage());
        }
    }
}
