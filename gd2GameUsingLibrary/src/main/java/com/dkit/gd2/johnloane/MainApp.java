package com.dkit.gd2.johnloane;

import com.dkit.gd2.patricknugent.game.*;


public class MainApp
{
    public static void main(String[] args)
    {
        Player adam = new Player("Adam", 10, 15, "Katana");
        System.out.println(adam);

        saveObject(adam);
    }

    public static void saveObject(ISavable objectToSave)
    {
        for(int i=0; i < objectToSave.write().size(); i++)
        {
            System.out.println("Saving " + objectToSave.write().get(i) + " to storage device");
        }
    }
}
