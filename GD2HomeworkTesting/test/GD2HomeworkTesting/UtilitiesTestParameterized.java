/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GD2HomeworkTesting;

import java.util.Collection;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import java.util.Arrays;
import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class UtilitiesTestParameterized 
{
    private Utilities util;
    
    private String input;
    private String output;
    
    public UtilitiesTestParameterized(String input, String output)
    {
        this.input = input;
        this.output = output;
    }
    
    @org.junit.Before
    public void setup()
    {
        util = new Utilities();
    }
    
    @Parameterized.Parameters
    public static Collection<Object[]> testConditions()
    {
        return Arrays.asList(new Object[][]{
            {"ABCDEFF", "ABCDEF"},
            {"AB88EFF", "AB8EF"},
            {"A", "A"},
            {"AAA", "A"}
    });
    }
    

    @org.junit.Test
    public void removePairs()
    {
        assertEquals(output, util.removePairs(input));
    }
    
}
