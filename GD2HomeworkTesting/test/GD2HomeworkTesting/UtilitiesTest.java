/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GD2HomeworkTesting;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author D00218208
 */
public class UtilitiesTest {
    
    public UtilitiesTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of everyNthChar method, of class Utilities.
     */
    @org.junit.Test
    public void testEveryNthCharNSmaller() {
        Utilities util = new Utilities();
        char[] result = util.everyNthChar(new char[]{'h','e','l','l','o'}, 2);
        assertArrayEquals(new char[]{'e','l'}, result);
    }
    
    @org.junit.Test
    public void testEveryNthCharNBigger() {
        Utilities util = new Utilities();
        char[] result = util.everyNthChar(new char[]{'h','e','l','l','o'}, 6);
        assertArrayEquals(new char[]{'e','l'}, result);
    }

    /**
     * Test of removePairs method, of class Utilities.
     */
    @org.junit.Test
    public void testRemovePairsAABCDDEEFF() {
        String source = "AABCDDEEFF";
        Utilities util = new Utilities();
        String expResult = "ABCDEF";
        String result = util.removePairs(source);
        assertEquals(expResult, result);
    }
    
    @org.junit.Test
    public void testRemovePairsABCCABDEEF() {
        String source = "ABCCABDEEF";
        Utilities util = new Utilities();
        String expResult = "ABCABDEF";
        String result = util.removePairs(source);
        assertEquals(expResult, result);
    }
    
    @org.junit.Test
    public void testRemovePairsNull() {
        String source = null;
        Utilities util = new Utilities();
        String expResult = "ABCABDEF";
        String result = util.removePairs(source);
        assertEquals(expResult, result);
    }


    /**
     * Test of converter method, of class Utilities.
     */
    @org.junit.Test
    public void testConverter() {
        Utilities instance = new Utilities();
        int expResult = 300;
        int result = instance.converter(10, 5);
        assertEquals(expResult, result);
    }
    
    @org.junit.Test(expected=ArithmeticException.class)
    public void testConverterArithmeticException() {
        Utilities instance = new Utilities();
        int expResult = 300;
        int result = instance.converter(10, 5);
        assertEquals(expResult, result);
    }

    /**
     * Test of nullIfOddLength method, of class Utilities.
     */
    @org.junit.Test
    public void testNullIfOddLengthOdd() {
        String source = "odd";
        Utilities instance = new Utilities();
        String result = instance.nullIfOddLength(source);
        assertNull(result);
    }
    
    public void testNullIfOddLengthEven() {
        String source = "even";
        Utilities instance = new Utilities();
        String result = instance.nullIfOddLength(source);
        assertNull(result);
    }
    
}
