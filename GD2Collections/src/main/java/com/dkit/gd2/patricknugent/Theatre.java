package com.dkit.gd2.patricknugent;

import java.util.*;

public class Theatre {
    private final String theatreName;
    private final int numRows;
    private final int seatsPerRow;
    public List<Seat> seats = new ArrayList<Seat>();

    static final Comparator<Seat> PRICE_ORDER;

    static
    {
        PRICE_ORDER = new Comparator<Seat>() {
            @Override
            public int compare(Seat seat1, Seat seat2)
            {
                if(seat1.getPrice() < seat2.getPrice())
                {
                    return -1;
                }
                else if(seat1.getPrice() == seat2.getPrice())
                {
                    return 0;
                }
                else
                {
                    return -1;
                }
            }
        };
    }

    public Theatre(String theatreName, int numRows, int seatsPerRow)
    {
        this.numRows = numRows;
        this.seatsPerRow = seatsPerRow;
        this.theatreName = theatreName;
        int lastRow = 'A' + (numRows-1);
        for(char row = 'A'; row <= lastRow; ++row)
        {
            for(int seatNum = 1; seatNum <= seatsPerRow; ++seatNum)
            {
                double price = 12.00;
                if((row < 'D' ) && (seatNum >=4 && seatNum <= 9))
                {
                    price = 14.00;
                }
                else if((row > 'F') || (seatNum < 4 || seatNum > 9))
                {
                    price = 7.00;
                }
                Seat seat = new Seat(row + String.format("%02d", seatNum));
                seats.add(seat);
            }
        }
    }

    public String getTheatreName()
    {
        return theatreName;
    }

    //This has 0(n) complexity
    //Worst case is we search 96 seats
    public boolean reserveSeat(String seatNumber)
    {
        int low = 0;
        int high = seats.size()-1;
        
        while(low <= high)
        {
            System.out.println(".");
            
        }
        
        Seat requestedSeat = null;
        for(Seat seat : seats)
        {
            System.out.print(".");
            if(seat.getSeatNumber().equals(seatNumber))
            {
                requestedSeat = seat;
                break;
            }
        }

        if(requestedSeat == null)
        {
            System.out.println("There is no seat " + seatNumber);
            return false;
        }

        return requestedSeat.reserve();
    }
    
    //Binary search - worst case log_2(numberSeats)
    //For 96 the worst case will be 7 searches, 7 < 96
    public boolean reserveSeatBS(String seatNumber)
    {
        int low = 0;
        int high = seats.size()-1;
        
        while(low <= high)
        {
            System.out.println(".");
            int mid = (low+high)/2;
            Seat midSeat = seats.get(mid);
            int cmp = midSeat.getSeatNumber().compareTo(seatNumber);
            
            if(cmp < 0)
            {
                low = mid + 1;
            }
            else if(cmp > 0)
            {
                high = mid - 1;
            }
            else
            {
                return seats.get(mid).reserve();
            }
        }
        System.out.println("There is no seat " + seatNumber);
        return false;
    }

    public void printSeats()
    {
        for(int i=0; i < seats.size(); i++)
        {
            if(i % seatsPerRow == 0 && i != 0)
            {
                System.out.println();
            }
            System.out.print(seats.get(i).getSeatNumber() + " ");
        }
        System.out.println("\n=========================================================");
    }

    public class Seat implements Comparable<Seat>
    {
        private String seatNumber;
        private double price;
        private boolean reserved = false;

        public Seat(String seatNumber, double price)
        {
            this.seatNumber = seatNumber;
            this.price = price;
        }

        public String getSeatNumber()
        {
            return seatNumber;
        }

        public double getPrice() {
            return price;
        }

        public boolean reserve()
        {
            if(reserved == true)
            {
                return false;
            }
            else
            {
                this.reserved = true;
                System.out.println("Seat" + seatNumber + " reserved");
                return true;
            }
        }

        public boolean cancel()
        {
            if(this.reserved == false)
            {
                return false;
            }
            else
            {
                this.reserved = false;
                System.out.println("Reservation of seat " + seatNumber + " cancelled");
                return true;
            }
        }
        
        @Override
        public int compareTo(Seat seat)
        {
            return this.seatNumber.compareToIgnoreCase(seat.getSeatNumber());
        }
    }

}
