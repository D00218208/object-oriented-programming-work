package com.dkit.gd2.patricknugent;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main
{
    public static void main(String[] args)
    {
        //System.out.println(('A' + 6));
        Theatre omniplexDundalk = new Theatre("Omniplex Dundalk", 8, 12);
        printList(omniplexDundalk.getSeats());
        //omniplexDundalk.printSeats();
        
        List<Theatre.Seat> seatCopy = new ArrayList<Theatre.Seat>(omniplexDundalk.seats);
        printList(omniplexDundalk.getSeats());
        Collections.sort(omniplexDundalk.getSeats(), Theatre.PRICE_ORDER);
        printList(omniplexDundalk.getSeats());
        
        seatCopy.get(1).reserve();

        Collections.shuffle(seatCopy);
        printList(seatCopy);
        
        bubbleSort(seatCopy);
        System.out.println("Printing sorted list");
        printList(seatCopy);
        
        //Homework: Try and brute force the magic square solution from earlier class (for Thursday)
        //Explain why the deep copy is throwing the IndexOutOfBoundsException (for Tuesday)
        //Find a fix
        List<Theatre.Seat> notADeepCopy = new ArrayList(omniplexDundalk.seats.size());
        for(int i=0; i < 96; ++i)
        {
            notADeepCopy.add(omniplexDundalk.new Seat("A01"));
        }
        Collections.copy(notADeepCopy, omniplexDundalk.seats);
        System.out.println(omniplexDundalk.seats);
        System.out.println(seatCopy);
        System.out.println(notADeepCopy);
        
        //seatCopy is a shallow copy of the seats from the omniplex
        //seatCopy and the original seats are two separate arraylists
        System.out.println("Printing the original seats");
        printList(omniplexDundalk.seats);
        
        Theatre.Seat minSeat = Collections.min(seatCopy);
        System.out.println("Min seat is " + minSeat.getSeatNumber());
        
        Theatre.Seat maxSeat = Collections.max(seatCopy);
        System.out.println("Max seat is " + maxSeat.getSeatNumber());
        
        if(omniplexDundalk.reserveSeat("A02"))
        {
            System.out.println("Please pay for A02");
        }
        else
        {
            System.out.println("Seat is already reserved");
        }
        
        if(omniplexDundalk.reserveSeat("A15"))
        {
            System.out.println("Please pay");
        }
        else
        {
            System.out.println("Sorry that seat either doesn't exist or is already booked");
        }
        
        if(omniplexDundalk.reserveSeatBS("A15"))
        {
            System.out.println("Please pay");
        }

        /*if(Theatre.cancel())
        {
            System.out.println("Cancelled successfully");
        }
        else
        {
            System.out.println("Sorry that seat either doesn't exist or has not been booked");
        }*/
        
        
    }
    
    public static void printList(List<Theatre.Seat> list)
    {
        for(Theatre.Seat seat : list)
        {
            System.out.print(" " + seat.getSeatNumber() + "€" + String.format("%.2f", seat.getPrice()));
        }
        System.out.println();
        System.out.println("====================================================");
    }
    
    //Order n^2 complexity - it will be slow with large lists.
    public static void bubbleSort(List<? extends Theatre.Seat> list)
    {
        for(int i = 0; i < list.size(); ++i)
        {
            for(int j = i+i; j < list.size(); ++j)
            {
                if(list.get(i).compareTo(list.get(j)) > 0)
                {
                    Collections.swap(list, i, j);
                }
            }
        }
    }
}
