package com.dkit.gd2.patricknugent;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;

public class Quiz
{
    public static void main(String[] args)
    {
        Map<String, String> questionsAndAnswers = new HashMap<>();
        Scanner input = new Scanner(System.in);

        questionsAndAnswers.put("Which European capital city is known as the City of Light?", "Paris");
        questionsAndAnswers.put("How many sides does a rhombus have?", "Four");
        questionsAndAnswers.put("What are kept in an apiary?", "Bees");
        questionsAndAnswers.put("On which planet is the Great Dark Spot?", "Neptune");
        questionsAndAnswers.put("What is the first letter of the Greek alphabet?", "Alpha");
        questionsAndAnswers.put("What movie directed by Ridley Scott was released in 1979?", "Alien");
        questionsAndAnswers.put("Gorgonzola cheese is from which country?", "Italy" );

        generateQuestion();
    }

    public static void generateQuestion()
    {
        Scanner keyboard = new Scanner(System.in);
        Random rand = new Random();

        int questionNum = rand.nextInt(7);
        System.out.println("\n" + questionsAndAnswers.get(questionsAndAnswers));
        String answer = keyboard.nextLine().toLowerCase();

        if (questionsAndAnswers.containsKey(answer))
        {
            System.out.println("Your answer is correct!");
        }
        else
        {
            System.out.println("Your answer is incorrect!");
        }
    }
}
