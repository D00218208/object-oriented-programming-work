package com.dkit.gd2.patricknugent;

import java.util.*;

public class MapDemo
{
    public static void main(String[] args)
    {
        Map<String, String> languages = new HashMap<>();
        languages.put("Java", "I don't know what to say about this one");
        languages.put("Javascript", "Loose language. Like Java, only worse");
        languages.put("C++", "Heeeeeellllllllllpppppppppppp, Yeeeeaaaaaayyyyyyyyy");
        languages.put("PHP", "The worst language ever, I'm glad its over");
        languages.put("Python", "I actually liked it");
        languages.put("MySql", "Easy class");

        languages.remove("PHP");

        if(languages.remove("Python", "An interesting language"))
        {
            System.out.println("Python removed");
        }
        else
        {
            System.out.println("Python not removed");
        }

        //Replace
        if(languages.replace("MySql", "Easy class", "My favourite class ever"))
        {
            System.out.println("MySql replaced");
        }
        else
        {
            System.out.println("MySql not replaced");
        }

        if(languages.containsKey("Java"))
        {
            System.out.println("Java is already in the map");
        }
        else
        {
            languages.put("Java", "Something about Java");
        }

        System.out.println(languages.put("Java", "My favourite language by far"));
        System.out.println(languages.put("Haskell", "A functional programming language"));

        printMap(languages);
    }

    public static void printMap(Map<String, String> mapToPrint)
    {
        for(String key : mapToPrint.keySet())
        {
            System.out.println(key + " : " + mapToPrint.get(key));
        }
    }

}
