/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GD2AutoBoxChallenge;

import java.util.ArrayList;

public class Bank 
{
    private static ArrayList<Branch> branches = new ArrayList<Branch>();
    
    public static boolean addBranch(Branch branch)
    {
        if(findBranch(branch.getName()) >= 0) //changing findBranch to static causes 'this' variable in bank class to become an error
        {
            System.out.println("Branch already exists");
            return false;
        }
        branches.add(branch);
        return true;
    }
    
    public static void addCustomerToBranch(Customer customer)
    {
        branches.add(Branch.addCustomer(customer));
    }
    
    public static void addTransactionToCustomer(Branch branch, Customer customer, double transaction)
    {
        branch.getCustomer().addAmount(transaction);
    }
    
    public static void listOfCustomersInBranch(Branch branch)
    {
        branch.customerList();
    }
    
    public String queryBranch(Branch branch)
    {
        if(findBranch(branch) >= 0)
        {
            return branch.getName();
        }
        return null;
    }

    public Branch queryBranch(String name)
    {
        int position = findBranch(name);
        if(position >=0)
        {
            return this.branches.get(position);
        }
        return null;
    }
    
    private int findBranch(Branch branch)
    {
        return this.branches.indexOf(branch);
    }

    private int findBranch(String branchName)
    {
        for(int i=0; i < branches.size(); ++i)
        {
            Branch branch = this.branches.get(i);
            if(branch.getName().equals(branchName))
            {
                return i;
            }
        }
        return -1;
    }

}

