/*
Create a simple banking aplication
There should be a bank class
The bank should have an arraylist of branches (dundalk, dublin, etc.)
Each branch should have an arraylist of customers 
The customer class should have an arraylist of doubles (transactions)
Customer:
Name and arraylist of doubles
Branch:
Needs to be able to add a new customer and initial transaction amount
Also need to add additional transactions for that customer and branch
Bank:
Add a new branch
Add a new customer to that branch with inital transaction
Add a transaction for an existing customer for that branch
Show a list of customers for a particular branch and optionally list their transactions
Demonstrate auto boxing and unboxing in your code
Add data validation
 */
package GD2AutoBoxChallenge;

import java.util.Scanner;

public class Main 
{
    private static Scanner sc = new Scanner(System.in);
    
    public static void main(String[] args)
    {
        boolean quit = false;
        
        
        while(quit == false)
        {
            System.out.println("\nEnter action: (6 to show available actions)> ");
            int action = sc.nextInt();
            sc.nextLine();
            
            switch(action)
            {
                case 0:
                    System.out.println("Shutting down");
                    quit = true;                  
                    break;
                case 1:
                    printBranch();
                    break;
                case 2:
                    addBranch();
                    break;
                case 3:
                    addCustomer();
                    break;
                case 4:
                    addTransaction();
                    break;
                case 5:
                    queryCustomer();
                    break;
                case 6: 
                    printActions();
                    break;
            }
        }
    }
    
    private static void printBranch()
    {
        System.out.println("Enter branch name: ");
        String branchName = sc.nextLine();
        Branch existingBranch = Bank.queryBranch(branchName); //changing queryBranch to static causes 'this' variable in bank class to become an error
        if(existingBranch == null)
        {
            System.out.println("Branch not found");
            return;
        }
        
        Bank.listOfCustomersInBranch(existingBranch);
    }
    
    private static void addBranch()
    {
        System.out.println("Enter new branch name: ");
        String name = sc.nextLine();
        Branch newBranch = new Branch(name);
        if(Bank.addBranch(newBranch))
        {
            System.out.println("New branch added: name " + name);
        }
        else
        {
            System.out.println("Cannot add as branch already exists");
        }
    }
    
    private static void addCustomer()
    {
        System.out.println("Enter new customer name: ");
        String name = sc.nextLine();
        System.out.println("Enter initial transaction amount: ");
        double amount = sc.nextDouble();
        Customer newCustomer = new Customer(name,amount);
        if(Branch.addCustomer(newCustomer))
        {
            System.out.println("New customer added: name ");
        }
        else
        {
            System.out.println("Cannot add as customer already exists");
        }
        System.out.println("Enter the branch associated with the customer: ");
        String branchName = sc.nextLine();
        
        Branch existingBranch = Bank.queryBranch(branchName); //changing queryBranch to static causes 'this' variable in bank class to become an error
        if(existingBranch == null)
        {
            System.out.println("Branch not found");
            return;
        }
        
        Bank.addCustomerToBranch(newCustomer);
    }
    
    private static void addTransaction()
    {
        System.out.println("Enter the branch associated with the customer: ");
        String branchName = sc.nextLine();       
        Branch existingBranch = Bank.queryBranch(branchName); //changing queryBranch to static causes 'this' variable in bank class to become an error
        if(existingBranch == null)
        {
            System.out.println("Branch not found");
            return;
        }
        
        System.out.println("Enter customer name: ");
        String name = sc.nextLine();
        System.out.println("Enter transaction amount: ");
        double amount = sc.nextDouble();
        
        Customer existingCustomer = Branch.queryCustomer(name);
        if(existingCustomer == null)
        {
            System.out.println("Customer not found");
            return;
        }
        
        Bank.addTransactionToCustomer(existingBranch, amount);   
    }
    
    private static void queryCustomer() 
    {
        System.out.println("Enter existing customer name: ");
        String name = sc.nextLine();
        Customer existingContactRecord = Branch.queryCustomer(name);
        if (existingContactRecord == null)
        {
            System.out.println("Customer not found.");
            return;
        }

        System.out.println("Name: " + existingContactRecord.getName() + " Transactions: " + existingContactRecord.getTransactions());
    }
    
    private static void printActions()
    {
        System.out.println("\nAvailable options: \npress");
        System.out.println("0 - to shutdown\n" +
                           "1 - to print all branches and customers\n" +
                           "2 - to add a new branch\n" +
                           "3 - to add customer to a branch\n" +
                           "4 - to add transaction to customer\n" +
                           "5 - to query whether customer exists\n" +
                           "6 - to print list of options");
    }
    
}
