/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GD2AutoBoxChallenge;

import java.util.ArrayList;

public class Customer {
    
    private String name;
    private ArrayList<Double> transactions ;

    public Customer(String name, double InitialTransaction)
    {
        this.name = name;
        this.transactions = new ArrayList<Double>();
        addAmount(InitialTransaction);
    }
    
    public void addAmount(double transaction)
    { 
        this.transactions.add(transaction);
    }

    public String getName()
    {
        return this.name;
    }
    
    public String getTransactions()
    {
        return this.transactions.toString();
    }
    
}

