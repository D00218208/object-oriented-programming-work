/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GD2AutoBoxChallenge;

import java.util.ArrayList;

public class Branch {
    private static ArrayList<Customer> customers;
    private String name;
    
    public Branch(String name)
    {
        this.name=name;
        customers=new ArrayList<Customer>();
    }
    
    public static boolean addCustomer(Customer customer)
    {
        if(findCustomer(customer.getName()) >= 0)
        {
            System.out.println("Contact already exists");
            return false;
        }
        customers.add(customer);
        return true;
    }
    
    public void addTransaction(double transaction,String name)
    {
        for(int i=0;i<customers.size();i++)
        {
            if(name.equals(customers.get(i).getName()))
            {
            customers.get(i).addAmount(transaction);
            }
        }
    }
    
    public void customerList()
    {
        for(int i=0;i<customers.size();i++)
        {
            System.out.println(customers.get(i));
        }
    }
    
    public String getName()
    {
        return this.name;
    }
    
    public void setName(String name)
    {
        this.name=name;
    }
    
    public Customer getCustomer(String name)
    {
        for(int i=0;i<customers.size();i++)
        {
            if(name.equals(customers.get(i).getName()))
            {
                return customers.get(i);
            }
            else
            {
                return null;
            }
        }
        return null;
    }
    
    public String queryCustomer(Customer customer)
    {
        if(findCustomer(customer) >= 0)
        {
            return customer.getName();
        }
        return null;
    }

    public static Customer queryCustomer(String name)
    {
        int position = findCustomer(name);
        if(position >=0)
        {
            return this.customers.get(position);
        }
        return null;
    }
    
    private int findCustomer(Customer customer)
    {
        return this.customers.indexOf(customer);
    }

    private static int findCustomer(String customerName)
    {
        for(int i=0; i < customers.size(); ++i)
        {
            Customer customer = this.customers.get(i);
            if(customer.getName().equals(customerName))
            {
                return i;
            }
        }
        return -1;
    }
}
