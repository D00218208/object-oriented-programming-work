package com.dkit.gd2.patricknugent;

import java.util.Arrays;

public final class ImmutableEmployee
{
    private final String id;
    private final String name;
    private final String photo;
    private final String department;
    private final int[] floorAccess;

    public ImmutableEmployee(String id, String name, String photo, String department, int[] floorAccess)
    {
        this.id = id;
        this.name = name;
        this.photo = photo;
        this.department = department;
        this.floorAccess = floorAccess;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPhoto() {
        return photo;
    }

    public String getDepartment() {
        return department;
    }

    public int[] getFloorAccess() {
        return floorAccess;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj)
        {
            return true;
        }
        //System.out.println("obj.getClass() is " + obj.getClass());
        //System.out.println("this.getClass() is " + this.getClass());
        if((obj == null) || (obj.getClass() != this.getClass()))
        {
            return false;
        }
        String objId = ((Employee)obj).getId();
        return this.id.equals(objId);
    }

    @Override
    public int hashCode()
    {
        //System.out.println("Hashcode called");
        return this.id.hashCode() + 57;
    }

    @Override
    public String toString() {
        return "ID = " + id + "\n" +  "Name = " + name + "\n" + "Photo = " + photo + "\n" + "Department = " + department + "\n" + "Floor Access = " + Arrays.toString(floorAccess) + "\n";
    }
}
