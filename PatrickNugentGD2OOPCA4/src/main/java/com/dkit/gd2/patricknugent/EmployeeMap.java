package com.dkit.gd2.patricknugent;

import java.util.HashMap;

public class EmployeeMap
{
    private final HashMap< String, Employee> map;

    public EmployeeMap()
    {
        this.map = new HashMap<>();
    }

    public void put(String id, Employee employee)
    {
        map.put(id, new Employee(employee));
    }

    public Employee get(String key)
    {
        return new Employee(map.get(key));
    }
}
