package com.dkit.gd2.patricknugent;

import java.util.HashMap;
import java.util.Map;


public class Question3
{
    public static void main(String[] args)
    {
        EmployeeMap employeeMap = new EmployeeMap();
        int[] itAccess = {0, 1, 2, 3};
        int[] receptionAccess = {0};
        int[] salesAccess = {3};
        int[] accountingAccess = {2};
        int[] generalOfficeAccess = {2};
        int[] productionAccess = {-1};

        Employee mauriceMoss = new Employee("FQL6256", "Maurice Moss", "Photo of Maurice Moss", "IT", itAccess);
        employeeMap.put("FQL6256", mauriceMoss);

        Employee royTrenneman = new Employee("KAQ1048", "Roy Trenneman", "Photo of Roy Trenneman", "IT", itAccess);
        employeeMap.put("KAQ1048", royTrenneman);

        Employee ashWilliams = new Employee("TLC5666", "Ash Williams", "Photo of Ash Williams", "Reception", receptionAccess);
        employeeMap.put("TLC5666", ashWilliams);

        Employee freddyKrueger = new Employee("FCK1234", "Freddy Krueger", "Photo of Freddy Krueger", "Sales", salesAccess);
        employeeMap.put("FCK1234", freddyKrueger);

        Employee alexMurphy = new Employee("ROB3785", "Alex Murphy", "Photo of Alex Murphy", "Production", productionAccess);
        employeeMap.put("ROB3785", alexMurphy);

        Employee jasonVoorhees = new Employee("MOM3770", "Jason Voorhees", "Photo of Jason Voorhees", "General Office", generalOfficeAccess);
        employeeMap.put("MOM3770", jasonVoorhees);

        Employee herbertWest = new Employee("HIM4127", "Herbert West", "Photo of Herbert West", "Accounting", accountingAccess);
        employeeMap.put("HIM4127", herbertWest);

        String idToSearch = scanCard(mauriceMoss);
        if (idToSearch != "Employee not found")
        {
            displayDetails(employeeMap, idToSearch);
        }
    }

    public static String scanCard(Employee employee)
    {
        if(employee.getId() != null)
        {
            return employee.getId();
        }
        else
        {
            return "Employee not found";
        }
    }

    public static void displayDetails(EmployeeMap employeeMap, String key)
    {
        Employee employee = employeeMap.get(key);
        if( employee == null )
        {
            System.out.println("Employee not found");
        }
        else
        {
            System.out.println(employee.toString());
        }
    }
}
