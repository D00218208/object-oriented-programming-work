package com.dkit.gd2.patricknugent;

import java.util.ArrayList;
import java.util.Comparator;

public class Identifier implements Comparable<Identifier>
{
    private String name;
    private int occurrences;
    private ArrayList lines;

    public Identifier(String name, int occurrences, ArrayList lines) {
        this.name = name;
        this.occurrences = occurrences;
        this.lines = lines;
    }

    public String getName() {
        return name;
    }

    public int getOccurrences() {
        return occurrences;
    }

    public ArrayList getLines() {
        return lines;
    }

    @Override
    public String toString()
    {
        return name + ' '  + '[' + occurrences + ']' + ' ' + lines.toString();
    }

    public static Comparator<Identifier> NameComparator = new Comparator<Identifier>()
    {
        @Override
        public int compare(Identifier identifier1, Identifier identifier2)
        {
            return identifier1.getName().toLowerCase().compareTo(identifier2.getName().toLowerCase());
        }
    };

    @Override
    public int compareTo(Identifier identifier) {
        return 0;
    }
}
