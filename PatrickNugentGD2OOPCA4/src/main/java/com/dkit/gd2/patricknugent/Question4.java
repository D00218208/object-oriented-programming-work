package com.dkit.gd2.patricknugent;

import java.util.HashMap;
import java.util.Map;

public class Question4
{
    public static void main(String[] args)
    {
        Map<String, ImmutableEmployee> employees = new HashMap<>();
        int[] itAccess = {0, 1, 2, 3};
        int[] receptionAccess = {0};
        int[] salesAccess = {3};
        int[] accountingAccess = {2};
        int[] generalOfficeAccess = {2};
        int[] productionAccess = {-1};

        ImmutableEmployee mauriceMoss = new ImmutableEmployee("FQL6256", "Maurice Moss", "Photo of Maurice Moss", "IT", itAccess);
        employees.put("FQL6256", mauriceMoss);

        ImmutableEmployee royTrenneman = new ImmutableEmployee("KAQ1048", "Roy Trenneman", "Photo of Roy Trenneman", "IT", itAccess);
        employees.put("KAQ1048", royTrenneman);

        ImmutableEmployee ashWilliams = new ImmutableEmployee("TLC5666", "Ash Williams", "Photo of Ash Williams", "Reception", receptionAccess);
        employees.put("TLC5666", ashWilliams);

        ImmutableEmployee freddyKrueger = new ImmutableEmployee("FCK1234", "Freddy Krueger", "Photo of Freddy Krueger", "Sales", salesAccess);
        employees.put("FCK1234", freddyKrueger);

        ImmutableEmployee alexMurphy = new ImmutableEmployee("ROB3785", "Alex Murphy", "Photo of Alex Murphy", "Production", productionAccess);
        employees.put("ROB3785", alexMurphy);

        ImmutableEmployee jasonVoorhees = new ImmutableEmployee("MOM3770", "Jason Voorhees", "Photo of Jason Voorhees", "General Office", generalOfficeAccess);
        employees.put("MOM3770", jasonVoorhees);

        ImmutableEmployee herbertWest = new ImmutableEmployee("HIM4127", "Herbert West", "Photo of Herbert West", "Accounting", accountingAccess);
        employees.put("HIM4127", herbertWest);

        String idToSearch = scanCard(mauriceMoss);
        if (idToSearch != "Employee not found")
        {
            displayDetails(employees, idToSearch);
        }
    }

    public static String scanCard(ImmutableEmployee employee)
    {
        if(employee.getId() != null)
        {
            return employee.getId();
        }
        else
        {
            return "Employee not found";
        }
    }

    public static void displayDetails(Map<String, ImmutableEmployee> employees, String key)
    {
        System.out.println(employees.get(key).toString());
    }
}
