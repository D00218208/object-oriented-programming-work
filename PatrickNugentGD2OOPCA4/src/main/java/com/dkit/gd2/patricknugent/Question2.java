package com.dkit.gd2.patricknugent;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class Question2
{
    public static void main(String[] args)
    {
        System.out.println("Identifier [count] [line numbers]");
        readIdentifiersFromJavaFile();
    }

    private static void readIdentifiersFromJavaFile()
    {
        try(Scanner scanner = new Scanner(new FileReader("JavaFile.java")))
        {
            scanner.useDelimiter("[^A-Za-z0-9_]+");
            String identifierName;
            List<Identifier> identifiers = new ArrayList<>();

            while (scanner.hasNext())
            {
                identifierName = scanner.next();

                boolean isDuplicate = checkDuplicateIdentifier(identifiers, identifierName);
                if(isDuplicate == true)
                {
                    continue;
                }

                int identifierCount = 0;
                String line;
                int lineCount = 0;
                ArrayList<Integer> lineOccurrences = new ArrayList<>();
                Scanner scanner2 = new Scanner(new FileReader("JavaFile.java"));
                while (scanner2.hasNextLine())
                {
                    lineCount++;
                    line = scanner2.nextLine();
                    if(line.contains(identifierName))
                    {
                        identifierCount++;
                        lineOccurrences.add(lineCount);
                    }
                }
                Identifier identifier = new Identifier(identifierName, identifierCount, lineOccurrences);
                identifiers.add(identifier);
            }
            displayIdentifierInfo(identifiers);
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }

    private static void displayIdentifierInfo(List identifiers)
    {
        Collections.sort(identifiers, Identifier.NameComparator);
        for(int i = 0; i < identifiers.size(); i++)
        {
            System.out.println(identifiers.get(i).toString());
        }
    }

    private static boolean checkDuplicateIdentifier(List<Identifier> identifiers, String name)
    {
        for(int i = 0; i < identifiers.size(); i++)
        {
            if(name.equals(identifiers.get(i).getName()))
            {
                return true;
            }
        }
        return false;
    }

}
