public class Main
{
    public static void main(String[] args)
    {
        boolean finished = false;

        while (finished == false)
        {
            int[] arr = {1, 2, 3, 4, 5};

            for (int j = 0; j < arr.length; j++)
            {
                arr[j] *= 2;
            }
            finished = true;
        }
    }
}