/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gd2interface;

public interface ITelephone
{
    void powerOn();
    void dial(int phoneNumber);
    void answer();
    boolean isRinging();
   
}
