/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gd2interface;

public class Main 
{
    public static void main(String[] args) 
    {
        ITelephone adamsPhone;
        adamsPhone = new DeskPhone(1234567);
        adamsPhone.powerOn();
        adamsPhone.dial(456789);
        ((DeskPhone) adamsPhone).setRinging(true);
        adamsPhone.answer();
        
        ITelephone jacksPhone;
        jacksPhone = new MobilePhone(1234567);
        jacksPhone.powerOn();
        jacksPhone.dial(3456789);
        ((MobilePhone)jacksPhone).setRinging(true);
        jacksPhone.answer();
    }
    
}
