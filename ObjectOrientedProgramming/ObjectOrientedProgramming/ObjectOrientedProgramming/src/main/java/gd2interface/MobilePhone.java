/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gd2interface;

public class MobilePhone implements ITelephone
{
    private int myNumber;
    private boolean isRinging;
    private boolean isOn = false;

    public MobilePhone(int myNumber)
    {
        this.myNumber = myNumber;
        this.isRinging = false;
    }
    
    @Override
    public void powerOn() 
    {
        this.isOn = true;
        System.out.println("Turning on the mobile phone");
    }

    @Override
    public void dial(int phoneNumber) 
    {
        if(isOn)
        {
            System.out.println("Now ringing " + phoneNumber + " on the mobile phone");
        }
        else
        {
            System.out.println("The phone is switched off");
        }
        
    }

    @Override
    public void answer() 
    {
        if(isOn && isRinging)
        {
            System.out.println("Answering the mobile phone");
            isRinging = false;
        }
        
    }

    @Override
    public boolean isRinging() 
    {
        return isRinging;      
    }
    
    public void setRinging(boolean ringing) 
    {
        isRinging = ringing;
    }        
    
    
}
