/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CompositionGame;

public class Player
{
    public String fullName;
    public int hitPoints;
    public String weapon;
    
    public void loseHealth(int damage)
    {
        this.hitPoints -= damage;
        if(this.hitPoints <= 0)
        {
            System.out.println("Player died");
            //Reduce number of lives
        }
    }
    
    public int healthRemaining()
    {
        return this.hitPoints;
    }
    
}

