/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CompositionGame;

public class EncapsulatedPlayer 
{
    private String name;
    private int hitPoints = 100;
    private String weapon;

    public EncapsulatedPlayer(String name, int hitPoints, String weapon) {
        this.name = name;
        if(hitPoints > 0 && hitPoints <= 100)
        {
            this.hitPoints = hitPoints;
        }
        this.weapon = weapon;
    }
    
    public void loseHealth(int damage)
    {
        this.hitPoints -= damage;
        if(this.hitPoints <= 0)
        {
            System.out.println("Player died");
            //Reduce number of lives
        }
    }

    public int getHitPoints() 
    {
        return hitPoints;
    }
    
    
    
    
}
