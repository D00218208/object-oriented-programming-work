/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Polymorphism;

public class HarryPotter extends Movie
{
    public HarryPotter()
    {
        super("Harry Potter");
    }
    
    @Override
    public String plot()
    {
        return "Boy with glasses goes to magic school";
    }
    
}
    
