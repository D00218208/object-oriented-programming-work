/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Polymorphism;

public class LordOfTheRings extends Movie
{
    public LordOfTheRings()
    {
        super("Lord of the Rings");
    }
    
    @Override
    public String plot()
    {
        return "Deliver the ring to the volcano";
    }
    
}
