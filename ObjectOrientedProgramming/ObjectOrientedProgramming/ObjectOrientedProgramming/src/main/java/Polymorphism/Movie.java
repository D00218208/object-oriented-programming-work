/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Polymorphism;

public class Movie 
{
    private String name;
    
    public Movie(String name)
    {
        this.name = name;
    }
    
    public String plot()
    {
        return "No plot here. I am the base class.";
    }
    
    public String getName()
    {
        return name;
    }
}
