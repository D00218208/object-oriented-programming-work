package RegularExpressions;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main
{
    public static void main(String[] args)
    {
        String string = "I am a string";
        System.out.println(string);

        String yourString = string.replaceAll("I am", "You are");
        System.out.println(yourString);

        String alphanumeric = "abcDeeeF12Ghhiiiiijk99z";
        System.out.println(alphanumeric.replaceAll(".","Y")); //. replace everything
        System.out.println(alphanumeric.replaceAll("^abcDeee", "YYY")); //^ replace start of string(has to match)

        String secondString = "abcDeeeF12abcDeeeGhhiiiiijk99z";
        System.out.println(secondString.replaceAll("^abcDeee", "YYY"));

        System.out.println(alphanumeric.matches("^hello"));
        System.out.println(alphanumeric.matches("^abc"));
        System.out.println(alphanumeric.matches("abcDeeeF12Ghhiiiiijk99z")); //matches returns true of strings match(full string)

        System.out.println(alphanumeric.replaceAll("ijk99z$", "The end")); //$ replace end
        System.out.println(alphanumeric.replaceAll("[aei]", "x")); //[aei] replace any letter that is a, e or i

        System.out.println(alphanumeric.replaceAll("[aei][Fj]", "i did it"));

        System.out.println("harry".replaceAll("[Hh]arry", "harry"));

        String newAlphanumeric = "abcDeeeF12abcDeeeGhhiiiiijk99z";
        System.out.println(newAlphanumeric.replaceAll("[^ej]", "X"));// a ^ in [] means "match anything thats not ..."
        System.out.println(newAlphanumeric.replaceAll("[a-f3-8]", "X")); //replace a-f and 3-8
        System.out.println(newAlphanumeric.replaceAll("(?i)[a-f3-8]", "X")); //case insensitive
        System.out.println(newAlphanumeric.replaceAll("[0-9]", "X")); // replace 0-9
        System.out.println(newAlphanumeric.replaceAll("\\d", "X")); //replace 0-9
        System.out.println(newAlphanumeric.replaceAll("\\D", "X")); //replaces anything thats no a number

        String hasWhitespace = "I have blanks and\ta tab, and also a newline\n";
        System.out.println(hasWhitespace);
        System.out.println(hasWhitespace.replaceAll("\\s",""));//remove gaps
        System.out.println(hasWhitespace.replaceAll("\t", " "));//remove \t
        System.out.println(hasWhitespace.replaceAll("\\S","x"));//replaces anything thats not a space or tab
        System.out.println(hasWhitespace.replaceAll("\\w", "x")); //replaces letters and digits
        System.out.println(hasWhitespace.replaceAll("\\b", "x"));//shows start and end or a word in string

        String thirdAlphanumeric = "abcDeeeF12Ghhiiiijkl99z";
        System.out.println(thirdAlphanumeric.replaceAll("^abcDe{3}","x"));//exactly 3 es
        System.out.println(thirdAlphanumeric.replaceAll("^abcDe+","x"));//any amount of es
        System.out.println(thirdAlphanumeric.replaceAll("^abcDe*","x"));//0 or more es
        System.out.println(thirdAlphanumeric.replaceAll("^abcDe{2,5}","x"));//between 2-5 es
        System.out.println();
        System.out.println(thirdAlphanumeric.replaceAll("h+i*j","Y"));
        System.out.println();

        StringBuilder htmlText = new StringBuilder("<h1>My favourite games</h1>");
        htmlText.append("<h2>Space Invaders</h2>");
        htmlText.append("<p>OMG this is the best game ever</p>");
        htmlText.append("<h2>Asteroids</h2>");
        htmlText.append("<p>FTL what an amazing game</p>");
        htmlText.append("<h2>Splatterhouse</h2>");
        htmlText.append("<p>check it out<p>");

        String h2Pattern = "<h2>";
        Pattern pattern = Pattern.compile((h2Pattern));
        Matcher matcher = pattern.matcher(htmlText);
        System.out.println(matcher.matches());

        matcher.reset();//must reset between each call
        int count = 0;
        while(matcher.find())
        {
            count++;
            System.out.println("Occurence " + count + ": " + matcher.start() + " to " + matcher.end());
        }

        String h2GroupPattern = "(<h2>.*?</h2>)"; //lazy: takes as little as possible
        Pattern groupPattern = Pattern.compile((h2GroupPattern));
        Matcher groupMatcher = groupPattern.matcher(htmlText);
        System.out.println(groupMatcher.matches());

        groupMatcher.reset();//must reset between each call

        while(groupMatcher.find())
        {
            System.out.println("Occurence: " + groupMatcher.group(1));
        }

        String h2TextGroups = "(<h2>)(.+?)(</h2>)";
        Pattern h2TextPattern = Pattern.compile((h2TextGroups));
        Matcher h2TextMatcher = h2TextPattern.matcher(htmlText);
        System.out.println(h2TextMatcher.matches());

        h2TextMatcher.reset();//must reset between each call

        while(h2TextMatcher.find())
        {
            System.out.println("Occurence: " + h2TextMatcher.group(2));
        }

        System.out.println("harry".replaceAll("[H|h]arry", "Larry"));

        String tvTest = "tstvtkt";
        String tNotVRegExp = "t(?!v)";
        Pattern tNotV = Pattern.compile(tNotVRegExp);
        Matcher tNotVMatcher = tNotV.matcher(tvTest);

        count = 0;
        while(tNotVMatcher.find())
        {
            count++;
            System.out.println("Occurence " + count + ": " + tNotVMatcher.start() + " to " + tNotVMatcher.end());
        }

        /*testUsPhoneRegex();
        testIrishMobileRegex();
        testVisaCards();

        challenge1();
        challenge2();
        challenge3();
        challenge4();
        challenge5();
        challenge6();
        challenge7();
        challenge8();
        challenge9();*/
        challenge10();
        challenge11();
        challenge12();
        challenge13();
        challenge14();
        challenge15();
        challenge16();
    }
    
    public static void challenge16()
    {
        //Need a regular expression for Eircode
        //7 characters. First is a letter, next two are digits except for D6W
        //Last 5 are characters
        //e.g A65 F4E2
        String challenge16 = "A65 F4E2";
        String challenge16exception = "D6W 1234";
        Pattern pattern = Pattern.compile("^([ACDEFHKNPRTVWXY]{1}[0-9]{1}[0-9W]{1}[\\ \\-]?[0-9ACDEFHKNPRTVWXY]{4})$");
        Matcher matcher = pattern.matcher(challenge16exception);
        System.out.println(matcher.matches());
    }
    
    public static void challenge15()
    {
        String challenge13 = "11111";
        String challenge14 = "11111-1111";
        //Write regex to match either
        System.out.println(challenge14.matches("^(\\d{5}(-\\d{4})?)$")); 
    }
    
    public static void challenge14()
    {
        String challenge14 = "11111-1111";
        System.out.println(challenge14.matches("^(\\d{5}-\\d{4})$"));  
    }
    
    public static void challenge13()
    {
        String challenge13 = "11111";
        System.out.println(challenge13.matches("^(\\d{5})$"));     
    }
    
    public static void challenge12()
    {
        String challenge12 = "{0,2}, {0,5}, {1,3}, {2,4}, {x,y}, {6,34}";
        //Same as challenge 11 but ensure that the entries are digits
        Pattern pattern = Pattern.compile("\\{(\\d+, \\s?\\d+)\\}");
        Matcher matcher = pattern.matcher(challenge12);
        while(matcher.find())
        {
            System.out.println("Occurrence: " + matcher.group(1));
        }
    }
    
    public static void challenge11()
    {
        String challenge11 = "{0,2}, {0,5}, {1,3}, {2,4}";
        //Pull out the numbers minus the curly brackets
        //Occurrence 0, 2
        //Occurrence 0, 5
        
        Pattern pattern = Pattern.compile("\\{(.+?)\\}");
        Matcher matcher = pattern.matcher(challenge11);
        while(matcher.find())
        {
            System.out.println("Occurrence: " + matcher.group(1));
        }
    }

    public static void challenge10()
    {
        System.out.println("Challenge 10");
        //print start and end of each occurrence of numbers
        String challenge10 = "abcd.135\tuvqz.7\ttzik.999\n";
        Pattern pattern = Pattern.compile("[A-Za-z]+\\.(\\d+)\\s");
        Matcher matcher = pattern.matcher(challenge10);
        while(matcher.find())
        {
            System.out.println("Occurrence: start = " + matcher.start(1) + " end = " + matcher.end());
        }
    }

    public static void challenge9()
    {
        System.out.println("Challenge 9");
        //write regex to print all the numbers
        String thing = "abcd.135\tuvqz.7\ttzik.999\n";
        String regex = "([\\d]+)";
        Pattern stringPattern = Pattern.compile(regex);
        Matcher stringMatcher = stringPattern.matcher(thing);

        while(stringMatcher.find())
        {
            System.out.println("Occurence: " + stringMatcher.group(1));
        }
    }

    public static void challenge8()
    {
        System.out.println("Challenge 8");
        //take in string, use pattern and matcher classes to identify and print each of the numbers
        String letters = "abcd.135uvqz.7tzik.999";
        String regex = "[a-zA-Z]+\\.([0-9]+)";

        Pattern stringPattern = Pattern.compile(regex);
        Matcher stringMatcher = stringPattern.matcher(letters);

        while(stringMatcher.find())
        {
            System.out.println("Occurence: " + stringMatcher.group(1));
        }
    }

    public static void challenge7()
    {
        System.out.println("Challenge 7");
        //allow any number of upper or lowercase letters followed by a . followed by any number of digits
        String word = "ancSFA.13523451";
        System.out.println(word.matches("^[a-zA-Z]+(\\.)[0-9]+$"));
    }

    public static void challenge6()
    {
        System.out.println("Challenge 6");
        //find exact match for challenge 5 string
        String letters = "aaabccccccccdddeffg";
        System.out.println(letters.matches("^[a]{3}b[c]{8}[d]{3}e[f]{2}g$"));
    }

    public static void challenge5()
    {
        System.out.println("Challenge 5");
        //aaabccccccccdddeffg
        //write shortest regex to match
        String letters = "aaabccccccccdddeffg";
        System.out.println(letters.matches("[a-g]+"));
    }

    public static void challenge4()
    {
        System.out.println("Challenge 4");
        //in the following sentence "replace all blanks with underscores"

        String sentence = "replace all blanks with underscores";
        System.out.println(sentence.replaceAll("\\s", "_"));
    }

    public static void challenge3()
    {
        System.out.println("challenge 3");
        //do same as challenge 2 but with pattern matcher
        String check = "I want a computer for Christmas";
        String check2 = "I want a dog for Christmas";
        String regexp = "I want a (computer|dog) for Christmas";
        Pattern stringPattern = Pattern.compile(regexp);
        Matcher stringMatcher = stringPattern.matcher(check2);
        System.out.println(stringMatcher.matches());
    }

    public static void challenge2()
    {
        //write regex to match either "I want a computer for Christmas"
        //or "I want a dog for Christmas

        String check = "I want a computer for Christmas";
        String check2 = "I want a dog for Christmas";
        System.out.println("Challenge 2");
        System.out.println(check.matches("(I want a computer for Christmas)?(I want a dog for Christmas)?"));
        System.out.println(check2.matches("(I want a computer for Christmas)?(I want a dog for Christmas)?"));

    }

    public static void challenge1()
    {
        //write regex to match "I want an Alienware Area51m for Christmas"
        String check = "I want an Alienware Area51m for Christmas";

        System.out.println("Challenge 1");
        System.out.println(check.matches("I want an Alienware Area51m for Christmas"));
        System.out.println(check.matches("^(I[ ]want[ ]an[ ]Alienware[ ]Area51m[ ]for[ ]Christmas)$"));
    }

    public static void testUsPhoneRegex()
    {
        String phone1 = "1234567890";//fail. Not correct
        String phone2 = "(123) 456-7890";//pass
        String phone3 = "123 456-7890";//fail. No brackets
        String phone4 = "(123)456-7890";//fail. No spaces

        System.out.println("USphone1 = " + phone1.matches("^[\\(]{1}[0-9]{3}[\\))]{1}[ ]{1}[0-9]{3}[\\-]{1}[0-9]{4}"));
        System.out.println("USphone2 = " + phone2.matches("^[\\(]{1}[0-9]{3}[\\))]{1}[ ]{1}[0-9]{3}[\\-]{1}[0-9]{4}"));
        System.out.println("USphone3 = " + phone3.matches("^[\\(]{1}[0-9]{3}[\\))]{1}[ ]{1}[0-9]{3}[\\-]{1}[0-9]{4}"));
        System.out.println("USphone4 = " + phone4.matches("^[\\(]{1}[0-9]{3}[\\))]{1}[ ]{1}[0-9]{3}[\\-]{1}[0-9]{4}"));
    }

    public static void testIrishMobileRegex()
    {
        //+3538[3,5,6,7,8,9][0-9]{7}
        String phone1 = "+353861234567";
        String phone2 = "(123)456-7890";
        String phone3 = "+353881234567";
        String phone4 = "353881234567";
        String phone5 = "+343861234567";
        String phone6 = "+3538612345675";
        String phone7 = "+35386123456";
        System.out.println("IRphone1 = " + phone1.matches("^([\\+]3538[35679][0-9]{7})$"));
        System.out.println("IRphone2 = " + phone2.matches("^([\\+]3538[35679][0-9]{7})$"));
        System.out.println("IRphone3 = " + phone3.matches("^([\\+]3538[35679][0-9]{7})$"));
        System.out.println("IRphone4 = " + phone4.matches("^([\\+]3538[35679][0-9]{7})$"));
        System.out.println("IRphone5 = " + phone5.matches("^([\\+]3538[35679][0-9]{7})$"));
        System.out.println("IRphone6 = " + phone6.matches("^([\\+]3538[35679][0-9]{7})$"));
        System.out.println("IRphone7 = " + phone7.matches("^([\\+]3538[35679][0-9]{7})$"));
    }

    public static void testVisaCards()
    {
        String visa1 = "4444444444444";
        String visa2 = "5444444444444";
        String visa3 = "4444444444444444";
        String visa4 = "4444";

        System.out.println("visa1 = " + visa1.matches("^(4[0-9]{12}([0-9]{3})?)$"));
        System.out.println("visa2 = " + visa2.matches("^(4[0-9]{12}([0-9]{3})?)$"));
        System.out.println("visa3 = " + visa3.matches("^(4[0-9]{12}([0-9]{3})?)$"));
        System.out.println("visa4 = " + visa4.matches("^(4[0-9]{12}([0-9]{3})?)$"));
    }
}
