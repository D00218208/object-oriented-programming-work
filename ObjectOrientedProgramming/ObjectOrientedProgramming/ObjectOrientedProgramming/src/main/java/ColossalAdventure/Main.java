/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ColossalAdventure;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Main 
{
    private static Locations locations = new Locations();
    
    public static void main(String[] args)
    {
        Scanner sc = new Scanner(System.in);
        Map<String, String> vocabulary = new HashMap<String, String>();
        vocabulary.put("QUIT", "Q");
        vocabulary.put("NORTH", "N");
        vocabulary.put("SOUTH", "S");
        vocabulary.put("EAST", "E");
        vocabulary.put("WEST", "W");
        vocabulary.put("REGISTER USER", "RU");
        vocabulary.put("LOG USER IN", "LUI");
        Map<String, Integer> tempExit = new HashMap<String, Integer>();
        
        locations.put(0, new Location(0, "You are sitting in front of a computer learning Java", null));
        tempExit.put("W", 2);
        tempExit.put("E", 3);
        tempExit.put("S", 4);
        tempExit.put("N", 5);  
        
        locations.put(1, new Location(1, "You are in your room", tempExit));
        tempExit.clear();
        tempExit.put("N", 5);
        
        locations.put(2, new Location(2, "You are in your hallway. There is a bathroom to your right", tempExit));
        tempExit.clear();
        tempExit.put("W", 1);
        
        locations.put(3, new Location(3, "You are in the bathroom, there is a mess on the floor", tempExit));
        tempExit.clear();
        tempExit.put("N", 1);
        tempExit.put("W", 2);
        
        locations.put(4, new Location(4, "You are in an empty room", tempExit));
        tempExit.clear();
        tempExit.put("S", 1);
        tempExit.put("W", 2);
        
        locations.put(5, new Location(5, "You are in the attic. There is a lot of clutter everywhere", tempExit));
        
        int loc = 1;
        
        while(true)
        {
            System.out.println(locations.get(loc).getDescription());
            if(loc == 0)
            {
                break;
            }
            
            Map<String, Integer> exits = locations.get(loc).getExits();
            System.out.println("Available exits are: ");
            for(String exit : exits.keySet())
            {
                System.out.println(exit + ", ");
            }
            System.out.println();
            
            String direction = sc.nextLine().toUpperCase();
            if(direction.length() > 1)
            {
                String[] words = direction.split(" ");
                for(String word : words)
                {
                    if(vocabulary.containsKey(word))
                    {
                        direction = vocabulary.get(word);
                        break;
                    }
                }
            }
            
            if(exits.containsKey(direction))
            {
                loc = exits.get(direction);
            }
            else
            {
                System.out.println("You cannot go that direction");
            }
        }
    }
    
}