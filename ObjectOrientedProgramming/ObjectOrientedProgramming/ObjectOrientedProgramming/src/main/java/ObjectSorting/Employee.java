/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ObjectSorting;

import java.util.Comparator;

public class Employee implements Comparable<Employee>
{
    private int id;
    private String name;
    private int age;
    private String position;
    private long salary;

    public Employee(int id, String name, int age, String position, long salary) 
    {
        this.id = id;
        this.name = name;
        this.age = age;
        this.position = position;
        this.salary = salary;
    }

    public int getId() 
    {
        return id;
    }

    public String getName() 
    {
        return name;
    }

    public int getAge() 
    {
        return age;
    }

    public String getPosition() 
    {
        return position;
    }

    public long getSalary() 
    {
        return salary;
    }

    @Override
    public String toString() {
        return "Employee{" + "id=" + id + ", name=" + name + ", age=" + age + ", position=" + position + ", salary=" + salary + '}';
    }
    
    @Override
    public int compareTo(Employee employee)
    {
        return (this.id - employee.id);
    }
    
    //Use comparator to sort employee by order of salary
    public static Comparator<Employee> SalaryComparator = new Comparator<Employee>()
    {
        @Override
        public int compare(Employee employeeOne, Employee employeeTwo)
        {
            return (int)(employeeOne.getSalary() - employeeTwo.getSalary());
        }        
    };
    
    public static Comparator<Employee> AgeComparator = new Comparator<Employee>()
    {
        @Override
        public int compare(Employee employeeOne, Employee employeeTwo)
        {
            return (int)(employeeOne.getAge() - employeeTwo.getAge());
        }        
    };
    
    public static Comparator<Employee> NameComparator = new Comparator<Employee>()
    {
        @Override
        public int compare(Employee employeeOne, Employee employeeTwo)
        {
            return employeeOne.getName().compareTo(employeeTwo.getName());
        }        
    };
}
