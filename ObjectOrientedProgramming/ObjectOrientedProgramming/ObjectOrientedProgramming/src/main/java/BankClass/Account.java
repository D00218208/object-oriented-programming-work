/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BankClass;

public class Account 
{   
    private String number;
    private double balance;
    private String customerName;
    private String customerEmail;
    private String customerPhoneNumber; 
    
    public Account()
    {
        this("0000", 0.00, "Default name", "Default address", "Default phone");
        System.out.println("Default constructor called");
    }
    
    public Account(String customerName, String customerEmailAddress, String customerPhoneNumber)
    {
        this("0000", 50.00, customerName, customerEmailAddress, customerPhoneNumber);
    }
    
    public Account(String number, double balance, 
            String customerName, String customerEmail, 
            String customerPhoneNumber)
    {
        System.out.println("Account constructor with parameters called");
        this.number = number;
        this.balance = balance;
        this.customerName = customerName;
        this.customerEmail = customerEmail;
        this.customerPhoneNumber = customerPhoneNumber;
    }
    
    public String getNumber() {
        return number;
    }

    public void setNumber(String number) 
    {
        this.number = number;
    }

    public double getBalance() 
    {
        return balance;
    }

    public void setBalance(double balance) 
    {
        this.balance = balance;
    }

    public String getCustomerName() 
    {
        return customerName;
    }

    public void setCustomerName(String customerName) 
    {
        this.customerName = customerName;
    }

    public String getCustomerEmail() 
    {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) 
    {
        this.customerEmail = customerEmail;
    }

    public String getCustomerPhoneNumber() 
    {
        return customerPhoneNumber;
    }

    public void setCustomerPhoneNumber(String customerPhoneNumber) 
    {
        this.customerPhoneNumber = customerPhoneNumber;
    }
    
    public void deposit(double depositAmount)
    {
        if(depositAmount > 0)
        {
            this.balance += depositAmount;
        }
        else
        {
            System.out.println("Deposit amount must be strictly positive");
        }
    }
    
    public void withdrawal(double withdrawalAmount)
    {
        if((withdrawalAmount > 0) && (withdrawalAmount <= this.balance))
        {
            this.balance -= withdrawalAmount;
            System.out.println("Withdrawal of " + withdrawalAmount + ". Current balance is: " + this.balance);
        }
        else if(withdrawalAmount <=0)
        {
            System.out.println("Please select a strictly positive withdrawal amount");
        }
        else
        {
            System.out.println("Only " + this.balance + " available. Withdrawal not possible");
        }
    }
}


