/* A vip has a name, creditlimit and emailAddress
Need 3 constructors - one that takes three parameters
One that takes two parameters and sets a default email address
Last one is a default constructor
Use constructor chaining
*/
package BankClass;

public class VIP 
{
    private String name;
    private double creditLimit;
    private String emailAddress;
    
    public VIP(String name, double creditLimit, String emailAddress)
    {
        this.name = name;
        this.creditLimit = creditLimit;
        this.emailAddress = emailAddress;
    }
    
    public VIP(String name, double creditLimit)
    {
        this(name, creditLimit, "unknown@gmail.com");
    }
    
    public VIP()
    {
        this("Default Name", 50.00, "Default@gmail.com");
    }
}


