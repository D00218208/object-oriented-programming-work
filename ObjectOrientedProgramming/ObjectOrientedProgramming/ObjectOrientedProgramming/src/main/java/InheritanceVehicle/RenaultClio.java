/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package InheritanceVehicle;

import java.util.ArrayList;
import java.util.Arrays;

public class RenaultClio extends Car
{
    private int breakDownAssistanceMonths;
    
    public RenaultClio(int breakDownAssistanceMonths) 
    {
        super("Renault Clio", "Small", "2WD", 102, new ArrayList<String>(Arrays.asList("Red", "Green")), 5, 5, "Hatchback", "1.1", false);
        this.breakDownAssistanceMonths = breakDownAssistanceMonths;
    }
    
    public void accelerate(int rate)
    {
        int newSpeed = getCurrentSpeed()+rate;
        if(newSpeed <= 0)
        {
            stop();
            changeGear("Neutral");
        }
        else if(newSpeed >0 && newSpeed <= 10)
        {
            changeGear("1");
        }
        else if(newSpeed >10 && newSpeed <= 20)
        {
            changeGear("2");
        }
        else if(newSpeed >20 && newSpeed <= 30)
        {
            changeGear("3");
        }
        else
        {
            changeGear("4");
        }
        
        if(newSpeed > 0)
        {
            changeVelocity(newSpeed, getCurrentDirection());
        }
    }
    
}
