/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package InheritanceVehicle;

import java.util.ArrayList;

public class Vehicle 
{
    private String name;
    private String size;
    private String terrain;
    
    private int maxSpeed;
    private ArrayList<String> colours;
    private int currentSpeed;
    private int currentDirection;
    
    public Vehicle(String name, String size, String terrain, int maxSpeed, ArrayList<String> colours) {
        this.name = name;
        this.size = size;
        this.terrain = terrain;
        this.maxSpeed = maxSpeed;
        this.currentSpeed = 0;
        this.currentDirection = 0;
        this.colours = colours;
    }
    
    public void steer(int direction)
    {
        this.currentDirection += direction;
        System.out.println("Vehicle.steer(): Steering at " + currentDirection + " degrees");
    }
    
    public void move(int speed, int direction)
    {
        this.currentSpeed = speed;
        this.currentDirection = direction;
        System.out.println("Vehicle.move(): Moving at " + currentSpeed + " in direction " + currentDirection);
    }
    
    public void stop()
    {
        this.currentSpeed = 0;
    }

    public String getName() {
        return name;
    }

    public String getSize() {
        return size;
    }

    public String getTerrain() {
        return terrain;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public int getCurrentSpeed() {
        return currentSpeed;
    }

    public int getCurrentDirection() {
        return currentDirection;
    }

    public ArrayList<String> getColours() {
        return colours;
    }
    
    
}
