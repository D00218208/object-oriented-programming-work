/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package InheritanceVehicle;

import java.util.ArrayList;

public class Car extends Vehicle
{
    private int doors;
    private int wheels;
    private String model;
    private String engine;
    private boolean isManual;
    private String currentGear;
    
    public Car(String name, String size, String terrain, int maxSpeed, ArrayList<String> colours, int doors, int wheels, String model, String engine, boolean isManual) {
        super(name, size, terrain, maxSpeed, colours);
        this.wheels = wheels;
        this.doors = doors;
        this.model = model;
        this.engine = engine;
        this.isManual = isManual;
        this.currentGear = "Neutral";
    }
    
    public void changeGear(String newGear)
    {
        this.currentGear = newGear;
        System.out.println("Car.changeGear() called. Gear changed to " + this.currentGear + " gear");
    }
    
    public void changeVelocity(int speed, int direction)
    {
        System.out.println("Car.changeVelocity() called: Speed: " + speed + " direction " + direction);
        move(speed, direction);
    }
    
    public void steer()
    {
        System.out.println("Vehicle.steer() called");
        super.steer(50);
    }
    
    public void move()
    {
        System.out.println("Vehicle.move() called");
        super.move(30, 20);
    }

    @Override
    public String toString() {
        return "Car{" + "doors=" + doors + ", wheels=" + wheels + ", model=" + model + ", engine=" + engine + '}' + super.toString();
    }
    
    
}
