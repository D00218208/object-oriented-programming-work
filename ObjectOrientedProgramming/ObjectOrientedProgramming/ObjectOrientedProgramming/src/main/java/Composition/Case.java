/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Composition;

public class Case 
{
    private String model;
    private String manufacturer;
    private String powerSupply;
    private String material;
    private int fans;
    //private Dimensions dimensions;

    public Case(String model, String manufacturer, String powerSupply, String material, int fans/*, Dimensions dimensions*/) {
        this.model = model;
        this.manufacturer = manufacturer;
        this.powerSupply = powerSupply;
        this.material = material;
        this.fans = fans;
        //this.dimensions = dimensions;
    }
    
    public void pressPowerButton()
    {
        System.out.println("Power button pressed");
    }

    public String getModel()
    {
        return model;
    }

    public String getManufacturer()
    {
        return manufacturer;
    }

    public String getPowerSupply()
    {
        return powerSupply;
    }

    public String getMaterial()
    {
        return material;
    }

    public int getFans()
    {
        return fans;
    }

    /*public Dimensions getDimensions()
    {
        return dimensions;
    }*/
    
    
    
}
