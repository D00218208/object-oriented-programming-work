/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Inheritance;

import java.util.ArrayList;

public class Main 
{
    public static void main(String[] args)
    {
        /*Animal animal = new Animal("Bob", 5, "Domestic", false, 2, 2, 1, 12, "Blue");
        animal.eat();
        animal.poo();
        animal.move();*/
                
        ArrayList<String> harrysColours = new ArrayList<String>();
        harrysColours.add("Orange");
        harrysColours.add("White");
        Dog harry = new Dog("Harry", 10, "Domestic", false, 1, 1, 0, 16, harrysColours, 4, "Mongrel", true, 50);
        
        harry.eat();
        System.out.println(harry.toString());
    }
}
