/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Inheritance;

import java.util.ArrayList;

public class Animal 
{
    private String name;
    private int age;
    private String habitat;
    private boolean wild;
    private int size;
    private int weight;
    private int dangerLevel;
    private int averageLifeSpan;
    private ArrayList<String> colour;
    
    public Animal(String name, int age, String habitat, boolean wildOrTame, int size, int weight, int dangerLevel, int averageLifeSpan, ArrayList<String> colour) 
    {
        this.name = name;
        this.age = age;
        this.habitat = habitat;
        this.wild = wildOrTame;
        this.size = size;
        this.weight = weight;
        this.dangerLevel = dangerLevel;
        this.averageLifeSpan = averageLifeSpan;
        this.colour = colour;
    }
    
    public void eat()
    {
        System.out.println("Animal.eat() called");
    }
    
    public void poo()
    {
        System.out.println("Animal.poo() called");
    }
    
    public void move()
    {
        System.out.println("Animal.move() called");
    }
    
    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public String getHabitat() {
        return habitat;
    }

    public boolean isWild() {
        return wild;
    }

    public int getSize() {
        return size;
    }
    
    public int getWeight() {
        return weight;
    }

    public int getDangerLevel() {
        return dangerLevel;
    }

    public int getAverageLifeSpan() {
        return averageLifeSpan;
    }
    
    public ArrayList<String> getColour() {
        return colour;
    }

    @Override
    public String toString() {
        return "Animal{" + "name=" + name + ", age=" + age + ", habitat=" + habitat + ", wild=" + wild + ", size=" + size + ", weight=" + weight + ", dangerLevel=" + dangerLevel + ", averageLifeSpan=" + averageLifeSpan + ", colour=" + colour + '}';
    }
    
    
    
    
}
