/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Inheritance;

import java.util.ArrayList;

public class Fish extends Animal 
{
    private int gills;
    private int eyes;
    private int fins;
    
    public Fish(String name, int age, String habitat, boolean wildOrTame, int size, int weight, int dangerLevel, int averageLifeSpan, ArrayList<String> colour, int gills, int eyes, int fins) {
        super(name, age, habitat, wildOrTame, size, weight, dangerLevel, averageLifeSpan, colour);
        this.gills = gills;
        this.eyes = eyes;
        this.fins = fins;
    }
    
    public void rest()
    {
        System.out.println("Fish.rest() called");
    }
    
    public void moveFins()
    {
        System.out.println("Fish.moveFins() called");
    }
    
    public void swim(int speed)
    {
        System.out.println("Fish.swim() called");
        moveFins();
        move();
    }

    @Override
    public String toString() {
        return "Fish{" + "gills=" + gills + ", eyes=" + eyes + ", fins=" + fins + "}" + super.toString();
    }
    
    
}

