package Generics;

import java.util.ArrayList;
import java.util.Collections;

public class Main 
{
    public static void main(String[] args)
    {
        /*ArrayList<Integer> items = new ArrayList<>();
        items.add(1);
        items.add(2);
        items.add(3);
        items.add(4);
        items.add(5);
        
        printDoubled(items);*/
        
        //Create a generic class to implement a league table for a sport.
        //The class should allow teams to be added to the list and store
        //a list of teams that belong to the league.
        
        //Your class shiuld have method to print out the teams in order,
        //with the team at the top of the league printed first
        
        //Only teams of the same type should be added to any particular instance of
        //the league class - the program should fail to compile if an attempt is
        //made to add an incompatible team
        SoccerPlayer ola = new SoccerPlayer("Ola");
        BasketballPlayer john = new BasketballPlayer("John");
        HurlingPlayer dj = new HurlingPlayer("DJ");
        
        Team<SoccerPlayer> dundalkFC = new Team("Dundalk FC");
        dundalkFC.addPlayer(ola);
        Team<BasketballPlayer> basketballTeam = new Team<>("Dundalk Basketball");
        basketballTeam.addPlayer(john);
        Team<HurlingPlayer> hurlingTeam = new Team<>("Dundalk hurling");
        hurlingTeam.addPlayer(dj);
        
        Team<SoccerPlayer> droghedaFC = new Team<>("Drogheda FC");
        
        dundalkFC.matchResult(droghedaFC, 10, 0);
        System.out.println("Wins for dundalkFC: " + dundalkFC.getWon());
        System.out.println("Losses for droghedaFC: " + droghedaFC.getLost());
        System.out.println("Dundalk ranking: " + dundalkFC.ranking());
        System.out.println("Drogheda ranking: " + droghedaFC.ranking());
        
        System.out.println(dundalkFC.compareTo(droghedaFC));
        Team<SoccerPlayer> mullingarFC = new Team<>("Mullingar FC");
        mullingarFC.matchResult(dundalkFC, 10, 0);
        mullingarFC.matchResult(droghedaFC, 10, 0);
        //dundalkFC.addPlayer(john);
        //dundalkFC.addPlayer(dj);
        
        //System.out.println(dundalkFC.numPlayers());
        ArrayList<Team> teams = new ArrayList<>();
        teams.add(dundalkFC);
        teams.add(droghedaFC);
        teams.add(mullingarFC);
        Collections.sort(teams);
        printTeams(teams);
    }
    
    private static void printTeams(ArrayList<Team> teams)
    {
        for(Team team : teams)
        {
            System.out.println(team.getName());
        }     
    }
    
    /*private static void printDoubled(ArrayList<Integer> n)
    {
        for(int i : n)
        {
            System.out.println(i*2);           
        }
    }*/
    
}
