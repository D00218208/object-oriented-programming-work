/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CarClass;

public class Main 
{
    public static void main(String[] args)
    {
        /* Create a new class for a bank account
        Create fields for the account number, balance,
        customer name, email and phone number
        Create getters and setters for each field
        Create two more methods
        1. Deposit should increment the balance
        2. Withdraw - take money out. If there are insufficient 
        funds don't allow the transaction to happen.
        */
        Car ferrari = new Car();
        ferrari.setModel("4587");
        System.out.println(ferrari.getModel());
    }
    
}
