/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CarClass;

public class Car
{
    //Note: edit multiple lines command (alt-shift)
    //Generate getters, setters etc: alt-insert
    public int doors;
    public int wheels;
    public String model;
    public String engine;
    public String colour;  
    
    public void setModel(String model)
    {
        String validModel = model.toLowerCase();
        if(validModel.equals("458") || validModel.equals("488 GTB"))
        {
            this.model = model;
        }
        else
        {
            this.model = "unknown";                        
        }
        
        this.model = model;
    }
    
    public String getModel()
    {
        return model;
    }
}
