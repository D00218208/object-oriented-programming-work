/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Exceptions;

import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class Main 
{
    public static void main(String[] args)
    {
        int x,y;
        boolean play = true;
        while(play)
        {
            try
            {
                x = getIntEAFP();
                y = getIntEAFP(); 
                System.out.println("x is " + x + " y is " + y);
                //System.out.println(divideLBYL(x,y));       
                System.out.println(divideEAFP(x,y));
                play = false;
            }
            catch(NoSuchElementException e)
            {
                System.out.println("You are clearly trying to break my program. Please don't enter control D if you want to use my application");
            }
        }
    }
    
    private static int divideLBYL(int x, int y)
    {
        if(y != 0)
        {
            return x/y;
        }
        else
        {
            System.out.println("y should be non-zero. Cannot divide by zero");
            return 0;
        }
    }
    
    private static int divideEAFP(int x, int y)
    {
        try{
           return x/y;
        }
        catch(ArithmeticException e)
        {
            System.out.println("y should be non-zero. Cannot divide by zero");
            return 0;
        }
    }
    
    private static int getInt()
    {
        Scanner sc = new Scanner(System.in);
        return sc.nextInt();
    }
    
    //This works other than control D
    //Bug: need a fix for control D error
    private static int getIntLBYLV1()
    {
        Scanner sc = new Scanner(System.in);
        if(sc.hasNextInt())
        {
            return sc.nextInt();
        }
        else
        {
            System.out.println("You did not enter an int");
            sc.remove();
            return getIntLBYLV1();
        }
    }
    
    //Bug control D - NoSuchElementException
    //Big integers cause parseInt() to NumberFormatException
    private static int getIntLBYLV2()
    {
        Scanner sc = new Scanner(System.in);
        boolean isValid = true;
        System.out.println("Please enter an integer ");
        String input = sc.next();
        for(int i=0; i<input.length(); i++)
        {
            if(Character.isDigit(input.charAt(i)) == false)
            {
                isValid = false;
                break;
            }
        }
        if(isValid)
        {
            return Integer.parseInt(input);
        }
        return getIntLBYLV2();
    }
    
    //Bug control D causes NoSuchElementException that we don't yet have a solution to
    private static int getIntEAFP() throws NoSuchElementException
    {
        Scanner sc = new Scanner(System.in);
        System.out.println("Please enter an integer. ^D will exit the application: ");
        try
        {
            return sc.nextInt();          
        }
        catch(InputMismatchException e)
        {
            System.out.println("You made a mistake");
            return getIntEAFP();
        }
        catch(NoSuchElementException e)
        {
            System.out.println("You entered control D");
            throw new NoSuchElementException("You entered control D");       
        }
    }
}
