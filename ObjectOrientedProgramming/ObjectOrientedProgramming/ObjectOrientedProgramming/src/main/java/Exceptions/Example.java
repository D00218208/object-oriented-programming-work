/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Exceptions;

import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class Example 
{
    private static Scanner sc = new Scanner(System.in);
    
    public static void main(String[] args)
    {
        try{
            int result = divide();
            System.out.println(result);
        }
        catch(NoSuchElementException e)
        {
            System.out.println(e.getMessage());        
        }  
    }
    
    private static int divide()
    {
        int x = 1; 
        int y = 1;
        try
        {
            x = getInt();
            y = getInt();
            System.out.println("x is " + x + " ,y is " + y);
            return x/y;
        }
        catch(NoSuchElementException e)
        {
            throw new NoSuchElementException("Dude, you entered the dreaded control D! That closes System.in and means we can't do anything else in this application");
        }
        
        catch(ArithmeticException e)
        {
            System.out.println("Please enter non-zero divisor");
            while(true){          
                y = getInt();
                if(y != 0)
                {
                    return x/y;                  
                }
                else
                {
                    System.out.println("Please enter non-zero divisor");
                }
            }      
        }
    }
        
    public static int getInt()
    {
        System.out.println("Please enter an integer ");
        while(true)
        {
            try{
               return sc.nextInt();                 
            }       
            catch(InputMismatchException e)
            {
                sc.nextLine();
                System.out.println("Please enter a number using only the digits 0 to 9");
            }
        }
    }
}
