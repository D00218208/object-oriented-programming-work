/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BankClass;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author D00218208
 */
public class AccountTest 
{
    private Account testAccount;
    
    @org.junit.Before
    public void setUp() throws Exception
    {
        testAccount = new Account("123", 100.00, "James", "james@gmail.com", "+353-1234567");
    }

    @org.junit.Test
    public void depositFifty()
    {
        testAccount.deposit(50);
        assertEquals(150.00, testAccount.getBalance(), 0.00000001);
    }
    
    @org.junit.Test
    public void depositZero()
    {
        testAccount.deposit(0.0);
        assertEquals(100.00, testAccount.getBalance(), 0.00000001);
    }
    
    @org.junit.Test
    public void depositMinusTen()
    {
        testAccount.deposit(-10);
        assertEquals(150.00, testAccount.getBalance(), 0.00000001);
    }
    
    @org.junit.Test
    public void withdrawalOneOOne()
    {
        testAccount.withdrawal(101.00);
        assertEquals(100.00, testAccount.getBalance(), 0.00000001);
    }
    
    @org.junit.Test
    public void withdrawalTen()
    {
        testAccount.withdrawal(10.00);
        assertEquals(100.00, testAccount.getBalance(), 0.00000001);
    }
    
    @org.junit.Test
    public void withdrawalZero()
    {
        testAccount.withdrawal(0.00);
        assertEquals(100.00, testAccount.getBalance(), 0.00000001);
    }
    
    @org.junit.Test
    public void withdrawalMax()
    {
        testAccount.withdrawal(100.00);
        assertEquals(0.00, testAccount.getBalance(), 0.00000001);
    }
    
}
