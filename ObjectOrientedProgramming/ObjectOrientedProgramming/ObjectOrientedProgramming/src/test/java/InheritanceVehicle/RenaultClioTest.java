/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package InheritanceVehicle;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author D00218208
 */
public class RenaultClioTest 
{
    
    @Test
    public void accelerateSpeed()
    {
        RenaultClio jacksClio = new RenaultClio(36);
        jacksClio.accelerate(100);
        assertEquals(100, jacksClio.getCurrentSpeed());
    }
    
    @Test
    public void accelerateGear()
    {
        RenaultClio jacksClio = new RenaultClio(36);
        jacksClio.accelerate(100);
        assertEquals("4", jacksClio.getCurrentSpeed());
    }
    
    
    
}
