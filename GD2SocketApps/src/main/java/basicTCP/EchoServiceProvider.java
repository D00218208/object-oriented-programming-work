package basicTCP;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class EchoServiceProvider
{
    public static void main(String[] args)
    {
        try
        {
            //Step 1: Set up a connection socket for other programs to connect to
            ServerSocket listeningSocket = new ServerSocket(EchoServiceDetails.LISTENING_PORT);

            boolean continueRunning = true;

            while(continueRunning)
            {
                //Step 2: Wait for incoming connections
                Socket dataSocket = listeningSocket.accept();

                //Step 3: Build the input the output streams
                OutputStream out = dataSocket.getOutputStream();
                PrintWriter output = new PrintWriter(new OutputStreamWriter(out));

                InputStream in = dataSocket.getInputStream();
                Scanner input = new Scanner(new InputStreamReader(in));

                String incomingMessage = "";
                while(!incomingMessage.equals("."))
                {
                    //Step 4: exchange messages
                    incomingMessage = input.nextLine();

                    System.out.println(incomingMessage);

                    output.println(incomingMessage);
                    output.flush();
                }
                dataSocket.close();
            }
            listeningSocket.close();
        }
        catch (IOException ioe)
        {
            System.out.println(ioe.getMessage());
        }


    }
}
