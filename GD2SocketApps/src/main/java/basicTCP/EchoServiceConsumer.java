package basicTCP;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class EchoServiceConsumer
{
    public static void main(String[] args)
    {
        try
        {
            //Step 1: establish a channel of communication
            Socket dataSocket = new Socket("localhost", EchoServiceDetails.LISTENING_PORT);

            //Step 2: Build output and input objects
            OutputStream out = dataSocket.getOutputStream();
            PrintWriter output = new PrintWriter(new OutputStreamWriter(out));

            InputStream in = dataSocket.getInputStream();
            Scanner input = new Scanner(new InputStreamReader(in));

            Scanner keyboard = new Scanner(System.in);
            String message = "";
            while(!message.equals("."))
            {
                System.out.println("Please enter a message: ");
                message = keyboard.nextLine();

                output.println(message);
                output.flush();

                String response = input.nextLine();
                System.out.println("Response: " + response);
            }
            dataSocket.close();
        }
        catch(UnknownHostException uhe)
        {
            System.out.println(uhe.getMessage());
        }
        catch(IOException ioe)
        {
            System.out.println(ioe.getMessage());
        }

    }
}
