
package mapencapsulatedstudent;

import java.util.ArrayList;
import java.util.List;

public class MapEncapsulatedStudent {

    public static void main(String[] args){
        
      StudentMap studentMap = new StudentMap();
      
      Student student = new Student("D001111", "Zoe");
      String key = "D001111";
      studentMap.put(key, student);
      
      student = new Student("D005555", "Shauna");
      key = "D005555";
      studentMap.put(key, student);
      
      student = new Student("D003333", "Vera");
      key = "D002222";
      studentMap.put(key, student);
    
      student.setName("Nick"); // change name in current Student object
                                // from "Vera" to "Nick"
      System.out.println("Student modified to: "+student);
      
      // Retrieve the Student D002222 from the StudentMap
      key = "D002222";
      student = studentMap.get(key);  // returns a CLONE of the student object
      if( student != null )
        System.out.println( "Student from the StudentMap: "+student );
      else
        System.out.println("Student id "+key+" was NOT found.");
      
      System.out.println("List of all students:");
      List<Student> list = studentMap.getAllStudents();
      for( Student s : list)
        System.out.println(s);
      
    }   
}