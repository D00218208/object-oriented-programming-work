/*
 * This class Encapsulates the HashMap that maps ids to student objects.
*  All the accessor methods - get(), put(), getAllStudents() clone
*  the Student objects so that access to the Student objects is permitted
*  only via the public methods.  
*  There will be no references from outside this object pointing to the
   Student objects in the Map, because this object creates clones of the originals.
   No references to the Student objetcs in the Map are returned from this object
   (as references to cloned Student objects are returned)
   
 */
package mapencapsulatedstudent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StudentMap {

  private final HashMap< String, Student> map;

  public StudentMap() {
    this.map = new HashMap<>();
  }

  /**
   * @param id
   * @param student - Student object, will be cloned
   * If we simply put() the student reference into the map, then the client code
   * will have direct access to the student object in the map.
   * Instead, this StudentMap creates its own clone (copy) of the student object passed in, 
   * and stores a reference top that object, thus eliminating access by the client.
   */
  public void put(String id, Student student) {
    map.put(id, new Student(student)); // put a Copy Constructed clone of Student object
  }

  /**
   * @param key
   * @return Student object, or null if key not found
   * If we simply return a reference to a Student object in the map, then the client
   * code will have a reference into the map, and could make changes. To prevent this,
   * we clone the Student object, and return a reference to it, thus preventing
   * access to the map contents.
   */
  public Student get(String key) {
    return new Student( map.get(key) ); // returns a Copy Constructed object
  }

  public List getAllStudents() {
    List<Student> list = new ArrayList<>();

    // get each Student value from map and add to list
    for (Map.Entry<String, Student> entry : map.entrySet()) {
      list.add(new Student( entry.getValue() ));  // clone and add to list
    }
    return list;
  }
}
