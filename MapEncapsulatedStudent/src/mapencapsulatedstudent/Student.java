/*
 * Student class with a Copy Constructor for Deep Copying
*/

package mapencapsulatedstudent;

public class Student {
  
    private String id;
    private String name;
    
    public Student(){}  // no arg constructor
    
    public Student(String id, String name){
        this.id = id;
        this.name = name;
    }
    
    //copy constructor
    public Student(Student student){
        this.id = student.id;
        this.name = student.name;
    }
    
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

  @Override
  public String toString() {
    return "Student{" + "id=" + id + ", name=" + name + '}';
  }
    

    
}