package client;

import core.VoteServiceDetails;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class VoteServiceClient
{
    public static void main(String[] args)
    {
        Socket dataSocket = null;

        try
        {
            //Step 1: Establish a channel of communication
            dataSocket = new Socket(VoteServiceDetails.SERVER_IP, VoteServiceDetails.LISTENING_PORT);

            //Step 2: Build the input and output streams
            OutputStream out = dataSocket.getOutputStream();
            PrintWriter output = new PrintWriter(new OutputStreamWriter(out), true);

            InputStream in = dataSocket.getInputStream();
            Scanner input = new Scanner(new InputStreamReader(in));

            Scanner keyboard = new Scanner(System.in);
            String message = "";
            boolean loggedIn = false;

            while (!loggedIn)
            {
                loggedIn = login(keyboard, input, output);
            }
            while (loggedIn && !message.equals(VoteServiceDetails.EXIT))
            {
                message = getChoice(keyboard);
                output.println(message);
                String response = input.nextLine();
                System.out.println(response);
            }
            System.out.println("Thank you for using the vote client");
        }
        catch (UnknownHostException uhe)
        {
            System.out.println(uhe.getMessage());
        }
        catch (IOException ioe)
        {
            System.out.println(ioe.getMessage());
        }
        finally
        {
            try
            {
                if(dataSocket != null)
                {
                    dataSocket.close();
                }
            }
            catch(IOException ioe)
            {
                System.out.println("Unable to disconnect " + ioe.getMessage());
                System.exit(1);
            }
        }

    }

    private static boolean login(Scanner keyboard, Scanner input, PrintWriter output)
    {
        String username;
        String password;
        String message;
        String response;

        System.out.println("Please enter your username: ");
        username = keyboard.nextLine();
        System.out.println("Please enter your password: ");
        password= keyboard.nextLine();

        //Send the login request to the server
        message = VoteServiceDetails.LOGIN+VoteServiceDetails.COMMAND_SEPARATOR+username+VoteServiceDetails.COMMAND_SEPARATOR+password;
        output.println(message);

        //Get the response from the server
        response = input.nextLine();
        if(response.equals(VoteServiceDetails.LOGIN_SUCCESSFUL))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private static String getChoice(Scanner keyboard)
    {
        String option = "";
        String vote = "";
        System.out.println("Options are 'Vote' or 'Exit'");
        option = keyboard.nextLine();
        if(option.equalsIgnoreCase("Vote"))
        {
            System.out.println("Options are 'Yes', 'No', 'Dunno'");
            vote = keyboard.nextLine();
            //Vote%Yes
            //exit
            return VoteServiceDetails.VOTE+VoteServiceDetails.COMMAND_SEPARATOR+vote;
        }
        else if(option.equalsIgnoreCase("Exit"))
        {
            return VoteServiceDetails.EXIT;
        }
        else
        {
            return getChoice(keyboard);
        }
    }

}
