package core;

public class VoteServiceDetails
{
    public final static String SERVER_IP = "localhost";
    public final static int LISTENING_PORT = 22517;

    //breaking characters
    public final static String COMMAND_SEPARATOR = "%";
    public final static String VOTE_REJECTED_SEPARATOR = "%%";

    //Command strings
    public final static String LOGIN = "login";
    public final static String VOTE = "vote";
    public final static String EXIT = "exit";

    public final static String LOGIN_SUCCESSFUL = "SUCCESSFUL";
    public final static String LOGIN_FAILED = "FAILED";
    public final static String VOTE_ACCEPTED = "ACCEPTED";
    public final static String VOTE_REJECTED = "REJECTED";
    public final static String EXIT_RESPONSE = "GOODBYE";
    public final static String VOTE_REJECTED_FULL_STRING =
            VOTE_REJECTED+VoteServiceDetails.COMMAND_SEPARATOR+"Do dogs dream?"+
            VoteServiceDetails.COMMAND_SEPARATOR+"Yes"+VoteServiceDetails.COMMAND_SEPARATOR;
}
