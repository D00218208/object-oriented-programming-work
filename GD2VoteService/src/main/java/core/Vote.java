package core;

import java.util.Objects;

public class Vote
{
    private String username;
    private String password;

    public Vote(String username, String password)
    {
        this.username = username;
        this.password = password;
    }

    public Vote(String username)
    {
        this.username = username;
    }

    public String getUsername()
    {
        return username;
    }

    public String getPassword()
    {
        return password;
    }

    @Override
    public String toString()
    {
        return "Vote{" +
                "username='" + username + '\'' +
                ", password='" + password +
                '}';
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vote vote = (Vote) o;
        return Objects.equals(getUsername(), vote.username);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(username);
    }

    public String format()
    {
        return username + VoteServiceDetails.BREAKING_CHARACTERS + password;
    }
}
