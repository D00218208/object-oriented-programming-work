package server;

import core.VoteServiceDetails;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class VoteServiceServer
{
    public static void main(String[] args)
    {
        ServerSocket listeningSocket = null;
        Socket dataSocket = null;
        Map<String, String> loginDetails = new HashMap<>();
        Set<String> responses = new HashSet<>();

        populateLoginDetails(loginDetails);
        populateResponses(responses);

        try
        {
            //Set up the listening socket
            listeningSocket = new ServerSocket((VoteServiceDetails.LISTENING_PORT));

            //Set up a ThreadGroup to manage all of the clients together
            ThreadGroup clientGroup = new ThreadGroup("Client threads");

            clientGroup.setMaxPriority(Thread.currentThread().getPriority()-1);

            //Do the main logic of the server
            boolean continueRunning = true;
            int threadCount = 0;

            while(continueRunning)
            {
                //Accept incoming connections
                dataSocket = listeningSocket.accept();

                threadCount++;
                System.out.println("The server has now accepted " + threadCount + "client");

                VoteServiceThread newClient = new VoteServiceThread(clientGroup,
                        dataSocket.getInetAddress()+"", dataSocket, threadCount,
                        loginDetails, responses);
                newClient.start();
            }
        }
        catch(IOException ioe)
        {
            System.out.println(ioe.getMessage());
        }
        finally
        {
            try
            {
                if(dataSocket != null)
                {
                    dataSocket.close();
                }
                if(listeningSocket != null)
                {
                    listeningSocket.close();
                }
            }
            catch(IOException ioe)
            {
                System.out.println("Unable to disconnect " + ioe.getMessage());
            }
        }
    }

    private static void populateLoginDetails(Map<String, String> loginDetails)
    {
        loginDetails.put("John", "Password");
        loginDetails.put("Marie", "secretPassword");
    }

    private static void populateResponses(Set<String> responses)
    {
        responses.put("Yes");
        responses.put("No");
        responses.put("Dunno");
    }
}
