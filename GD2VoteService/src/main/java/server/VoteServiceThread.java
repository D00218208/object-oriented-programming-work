package server;

import core.VoteServiceDetails;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.Set;

public class VoteServiceThread extends Thread
{
    private Socket dataSocket;
    private Scanner input;
    private PrintWriter output;
    private Map<String, String> loginDetails;
    private Set<String> responses;
    private int number;

    public VoteServiceThread(ThreadGroup group, String name, Socket dataSocket, int number,
                              Map<String, String> loginDetails, Set<String> responses)
    {
        super(group, name);

        try
        {
            this.dataSocket = dataSocket;
            this.number = number;
            input = new Scanner(new InputStreamReader(this.dataSocket.getInputStream()));
            output = new PrintWriter(this.dataSocket.getOutputStream(), true);
            this.loginDetails = loginDetails;
            this.responses = responses;
        }
        catch(IOException ioe)
        {
            System.out.println(ioe.getMessage());
        }
    }

    @Override
    public void run()
    {
        String incomingMessage = "";
        String response;

        try
        {
            while(!incomingMessage.equals(VoteServiceDetails.EXIT))
            {
                response = null;

                //Take the input from the client
                incomingMessage = input.nextLine();
                System.out.println("Received message: " + incomingMessage);

                //Break the incomingMessage into components
                String[] components = incomingMessage.split(VoteServiceDetails.COMMAND_SEPARATOR);

                if(components[0].equals(VoteServiceDetails.VOTE))
                {
                    //vote%yes
                    if(components.length == 2)
                    {
                        if(responses.contains(components[1]))
                        {
                            response = VoteServiceDetails.VOTE_ACCEPTED;
                        }
                        else
                        {
                            response = VoteServiceDetails.VOTE_REJECTED_FULL_STRING;
                        }
                    }
                    else
                    {
                        response = VoteServiceDetails.VOTE_REJECTED_FULL_STRING;
                    }
                }
                else if(components[0].equals(VoteServiceDetails.LOGIN))
                {
                    if(components.length == 3)
                    {
                        if(usernameAndPasswordCorrect(components))
                        {
                            response = VoteServiceDetails.LOGIN_SUCCESSFUL;
                        }
                        else
                        {
                            response = VoteServiceDetails.LOGIN_FAILED;
                        }
                    }
                    else
                    {
                        response = VoteServiceDetails.LOGIN_FAILED;
                    }
                }

                System.out.println("Sending response: " + response);
                output.println(response);
            }
            if(incomingMessage.equals(VoteServiceDetails.EXIT))
            {
                output.println(VoteServiceDetails.EXIT_RESPONSE);
            }
        }
        catch (NoSuchElementException nse)
        {
            System.out.println(nse.getMessage());
        }
        finally
        {
            try
            {
                System.out.println("\n Closing connection with client #" + number
                + "...");
                if(dataSocket != null)
                {
                    dataSocket.close();
                }
            }
            catch (IOException ioe)
            {
                System.out.println("Unable to disconnect: " + ioe.getMessage());
                System.exit(1);
            }
        }
    }

    private boolean usernameAndPasswordCorrect(String[] components)
    {
        if(loginDetails.containsKey(components[1]))
        {
            if(loginDetails.get(components[1]).equals(components[2]))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
}
