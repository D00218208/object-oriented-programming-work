package DTO;

/* The DTO is used to transfer data between the DAO and the Business Object.
    Here it represents a row of data from the user database table.
    The DAO fills a User object (DTO) with data retrieved from the resultSet
    and passes the User object to the business layer
    We can also pass collections of DTOs e.g. ArrayList<User>
*/

public class User 
{
    private int id;
    private String firstname;
    private String lastname;
    private String username;
    private String password;

    public User(int id, String firstname, String lastname, String username, String password) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.username = username;
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", firstname=" + firstname + ", lastname=" + lastname + ", username=" + username + ", password=" + password + '}';
    }
    
    
}
